<?php

use Illuminate\Database\Seeder;

class UsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('tbl_users')->insert([
            [
                'id'=>1,
                'name' => 'tecnologia',
                'email' => 'tecnologia@app.com',
                'password' => bcrypt('123456'),
                'i_fk_tipo_documento' => 53,
                'i_cedula' => 454,
                'vc_segundo_nombre' => 'root',
                'vc_primer_apellido' => 'root',
                'vc_segundo_apellido' => 'root',
                'dt_fecha_nacimiento' => '2000-01-01',
                'i_fk_ciudad' => 6,
                'i_fk_genero' => 14,
                'i_fk_etnia' => 3,
                'i_fk_eps' => 4,
                'i_fk_area' => 29,
                'vc_rh' => "fgfd",
                'vc_telefono'=> "fgfd",
                'vc_celular'=> "fgfd",
                'vc_direccion'=> "fgfd",
                'vc_estado' => 1,
            ],
            [
                'id'=>2,
                'name' => 'ApoyoTecnologia',
                'email' => 'apoyotecnologia@app.com',
                'password' => bcrypt('123456'),
                'i_fk_tipo_documento' => 53,
                'i_cedula' => 454,
                'vc_segundo_nombre' => 'ApoyoTecnologia',
                'vc_primer_apellido' => '',
                'vc_segundo_apellido' => 'root',
                'dt_fecha_nacimiento' => '2000-01-01',
                'i_fk_ciudad' => 6,
                'i_fk_genero' => 14,
                'i_fk_etnia' => 3,
                'i_fk_eps' => 4,
                'i_fk_area' => 29,
                'vc_rh' => "fgfd",
                'vc_telefono'=> "fgfd",
                'vc_celular'=> "fgfd",
                'vc_direccion'=> "fgfd",
                'vc_estado' => 1,
            ],
            [
                'id'=>3,
                'name' => 'juridico',
                'email' => 'juridico@app.com',
                'password' => bcrypt('123456'),
                'i_fk_tipo_documento' => 2,
                'i_cedula' => 454,
                'vc_segundo_nombre' => 'juridico',
                'vc_primer_apellido' => 'root',
                'vc_segundo_apellido' => 'root',
                'dt_fecha_nacimiento' => '2000-01-01',
                'i_fk_ciudad' => 6,
                'i_fk_genero' => 14,
                'i_fk_etnia' => 3,
                'i_fk_eps' => 4,
                'i_fk_area' => 50,
                'vc_rh' => "fgfd",
                'vc_telefono'=> "fgfd",
                'vc_celular'=> "fgfd",
                'vc_direccion'=> "fgfd",
                'vc_estado' => 1,
            ],
            [
                'id'=>4,
                'name' => 'recursohumano',
                'email' => 'recursohumano@app.com',
                'password' => bcrypt('123456'),
                'i_fk_tipo_documento' => 2,
                'i_cedula' => 454,
                'vc_segundo_nombre' => 'recursohumano',
                'vc_primer_apellido' => 'root',
                'vc_segundo_apellido' => 'root',
                'dt_fecha_nacimiento' => '2000-01-01',
                'i_fk_ciudad' => 6,
                'i_fk_genero' => 14,
                'i_fk_etnia' => 3,
                'i_fk_eps' => 4,
                'i_fk_area' => 31,
                'vc_rh' => "fgfd",
                'vc_telefono'=> "fgfd",
                'vc_celular'=> "fgfd",
                'vc_direccion'=> "fgfd",
                'vc_estado' => 1,
            ],
            [
                'id'=>5,
                'name' => 'apoyorecursohumano',
                'email' => 'apoyorecursohumano@app.com',
                'password' => bcrypt('123456'),
                'i_fk_tipo_documento' => 2,
                'i_cedula' => 454,
                'vc_segundo_nombre' => 'apoyorecursohumano',
                'vc_primer_apellido' => 'root',
                'vc_segundo_apellido' => 'root',
                'dt_fecha_nacimiento' => '2000-01-01',
                'i_fk_ciudad' => 6,
                'i_fk_genero' => 14,
                'i_fk_etnia' => 3,
                'i_fk_eps' => 4,
                'i_fk_area' => 31,
                'vc_rh' => "fgfd",
                'vc_telefono'=> "fgfd",
                'vc_celular'=> "fgfd",
                'vc_direccion'=> "fgfd",
                'vc_estado' => 1,
            ],
            [
                'id'=>6,
                'name' => 'almacen',
                'email' => 'almacen@app.com',
                'password' => bcrypt('123456'),
                'i_fk_tipo_documento' => 2,
                'i_cedula' => 454,
                'vc_segundo_nombre' => 'almacen',
                'vc_primer_apellido' => 'root',
                'vc_segundo_apellido' => 'root',
                'dt_fecha_nacimiento' => '2000-01-01',
                'i_fk_ciudad' => 6,
                'i_fk_genero' => 14,
                'i_fk_etnia' => 3,
                'i_fk_eps' => 4,
                'i_fk_area' => 17,
                'vc_rh' => "fgfd",
                'vc_telefono'=> "fgfd",
                'vc_celular'=> "fgfd",
                'vc_direccion'=> "fgfd",
                'vc_estado' => 1,
            ],
            //Usuarios solicitantes
            [
                'id'=>7,
                'name' => 'Steven',
                'email' => 'carlos@app.com',
                'password' => null,
                'i_fk_tipo_documento' => 2,
                'i_cedula' => 1073157928,
                'vc_segundo_nombre' => null,
                'vc_primer_apellido' => 'Hernandez',
                'vc_segundo_apellido' => 'Rios',
                'dt_fecha_nacimiento' => '2000-01-01',
                'i_fk_ciudad' => 6,
                'i_fk_genero' => 14,
                'i_fk_etnia' => 3,
                'i_fk_eps' => 4,
                'i_fk_area' => 29,
                'vc_rh' => "O+",
                'vc_telefono'=> "3138728391",
                'vc_celular'=> "3138728391",
                'vc_direccion'=> "cra 10 No 6 27",
                'vc_estado' => 1,
            ],
            [
                'id'=>8,
                'name' => 'Maria',
                'email' => 'maria@app.com',
                'password' => null,
                'i_fk_tipo_documento' => 2,
                'i_cedula' => 15482545,
                'vc_segundo_nombre' => 'Maria',
                'vc_primer_apellido' => 'Rios',
                'vc_segundo_apellido' => 'Rios',
                'dt_fecha_nacimiento' => '2000-01-01',
                'i_fk_ciudad' => 6,
                'i_fk_genero' => 14,
                'i_fk_etnia' => 3,
                'i_fk_eps' => 4,
                'i_fk_area' => 29,
                'vc_rh' => "O+",
                'vc_telefono'=> "3138728391",
                'vc_celular'=> "3138728391",
                'vc_direccion'=> "cra 10 No 6 27",
                'vc_estado' => 1,
            ]
            ,
            [
                'id'=>9,
                'name' => 'Luis',
                'email' => 'luis@app.com',
                'password' => null,
                'i_fk_tipo_documento' => 2,
                'i_cedula' => 12045145,
                'vc_segundo_nombre' => 'Alberto',
                'vc_primer_apellido' => 'Cortes',
                'vc_segundo_apellido' => 'Rios',
                'dt_fecha_nacimiento' => '2000-01-01',
                'i_fk_ciudad' => 6,
                'i_fk_genero' => 14,
                'i_fk_etnia' => 3,
                'i_fk_eps' => 4,
                'i_fk_area' => 50,
                'vc_rh' => "O+",
                'vc_telefono'=> "3138728391",
                'vc_celular'=> "3138728391",
                'vc_direccion'=> "cra 10 No 6 27",
                'vc_estado' => 1,
            ],
            [
                'id'=>10,
                'name' => 'Cristian',
                'email' => 'camilocalderont@gmail.com',
                'password' => bcrypt('123456'),
                'i_fk_tipo_documento' => 1,
                'i_cedula' => 1012383991,
                'vc_segundo_nombre' => 'Camilo',
                'vc_primer_apellido' => 'Calderón',
                'vc_segundo_apellido' => 'Tapia',
                'dt_fecha_nacimiento' => '1992-01-11',
                'i_fk_ciudad' => 2,
                'i_fk_genero' => 5,
                'i_fk_etnia' => 3,
                'i_fk_area' => 50,
                'i_fk_eps' => 4,
                'vc_rh' => "A+",
                'vc_telefono'=> "3795750 ext 4305",
                'vc_celular'=> "3057467347",
                'vc_direccion'=> "CR 8 15 46",
                'vc_estado' => 1,
            ]
        ]);
    }
}
