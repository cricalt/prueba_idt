<?php

use Illuminate\Database\Seeder;

class DetallesConcursoActividadEstimuloSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_parametros_detalles')->insert([
            [
                'vc_parametro_detalle' => 'BECA COMPLETA',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BECA EQUIVALENTE AL 60% DEL VALOR DE LA MATRICULA',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'DIRECTORES CON TRAYECTORIA',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'NOVELES DIRECTORES',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'FORMACIÓN',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LABORATORIOS PRÁCTICAS ARTÍSTICAS EXPERIMENTALES EN EL PARQUEADERO 45 SALÓN NACIONAL DE ARTISTAS',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PROGRAMACIÓN CONTINUA',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA A: RESIDENCIA NACIONAL EN DANZA DE TRADICIÓN Y FOLCLOR',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA B: RESIDENCIA NACIONAL EN DANZA CONTEMPORÁNEA',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA C: RESIDENCIA INTERNACIONAL EN DANZA CONTEMPORÁNEA',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA A: CATEGORÍA VARIACIONES JÓVENES SOLISTAS',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA B: CATEGORÍA VARIACIONES SOLISTAS',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA C: CATEGORÍA OBRA DE BALLET PARA AGRUPACIÓN',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA: CUADERNOS DE FOTOGRAFÍA',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA: LIBROS DEL REVÉS',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA: MODALIDAD CUADERNOS DE CÓMIC Y DIBUJO',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA 1: FESTIVALES CON TRAYECTORIA EN LA CONVOCATORIA',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA 2: FESTIVALES QUE PARTICIPAN  POR PRIMERA VEZ',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MODALIDAD 1',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MODALIDAD 2',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MODALIDAD 3',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA A: PROPUESTAS DE CIRCULACIÓN LOCAL CON MÚSICOS EN VIVO',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA B: PROPUESTAS DE CIRCULACIÓN LOCAL',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA A: AGRUPACIONES DE CORTA TRAYECTORIA ',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA B:AGRUPACIONES DE MEDIANA Y LARGA TRAYECTORIA',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA A:PROYECTOS EN LENGUAJES CONTEMPORÁNEOS',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA B:PROYECTOS EN LENGUAJES URBANOS',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA C: PROYECTOS  EN LENGUAJES MODERNOS , CLÁSICOS Y O NEOCLÁSICOS',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA A: CIRCULACIÓN  ARTÍSTICA',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA B: CIRCULACIÓN ACADÉMICA',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA C:CIRCULACIÓN NACIONAL CON PLATAFORMAS ESTABLECIDAS',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA A:REDES Y PLATAFORMAS DE CIRCULACIÓN DE FOLCLOR NACIONAL Y O INTERNACIONAL',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA B: FESTIVALES , PLATAFORMAS Y O ENCUENTROS DE DANZAS DEL MUNDO',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA A: NÚMERO DE CIRCO',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA B: OBRA O ESPECTÁCULO DE CIRCO',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA: ALEMÁN',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA: INGLÉS',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA: RESIDENCIA ESTUDIOS ARTÍSTICOS EN BLOQUE CIUDAD DE BOGOTÁ',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA: RESIDENCIAS EN BLOQUE 45 SALÓN NACIONAL DE ARTISTAS :EL REVÉS DE LA TRAMA',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA 1: PRÁCTICAS ARTÍSTICAS PARA UNA VIDA LIBRE SE SEXISMOS',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA 2: PRÁCTICAS ARTÍSTICAS DE LAS PERSONAS DISCAPACITADAS',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA 3: PRÁCTICAS ARTÍSTICAS ORIENTADAS A PREVENIR SITUACIONES DE VULNERACIÓN DE DERECHOS HUMANOS',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA 4: PRÁCTICAS ARTÍSTICAS PARA LA REPARACIÓN INTEGRAL A LAS VÍCTIMAS DEL CONFLICTO ARMADO INTERNO',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA 5: PRÁCTICAS ARTÍSTICAS COMO BIEN PÚBLICO',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA A: BATALLA INTERNACIONAL VIALTERNA BIVA CREW 5 VS 5',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA B: PRESENTADOR Y DJ BATALLA INTERNACIONAL VIA ALTERNA BIVA  ',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA: NACIONAL',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA: INTERNACIONAL',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA: GRAN FORMATO ',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA: MICROFORMATO',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA: COMUNITARIOS',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA: EMERGENTES',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA: INDEPENDIENTES',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORIA :CREACIÓN PRODUCCIÓN DE OBRA O EXPERIENCIA',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA A: SHOW DANZA URBANA, CUALQUIER ESTILO',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA B:BATALLA 1VS1 PARA CIRCULACIÓN INTERNACIONAL BBOY',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA C: BATALLA 1 VS 1 PARA CIRCULACIÓN INTERNACIONAL BGIRL',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA: DJ',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CATEGORÍA: MC',
                'i_fk_id_parametro' => 35,
                'i_estado' => 1,
            ],
        ]);
    }
}
