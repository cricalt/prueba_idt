<?php

use Illuminate\Database\Seeder;

class UsuarioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_users')->insert([
            'name' => 'Cristian',
            'email' => 'camilocalderont@gmail.com',
            'password' => bcrypt('123456'),
            'i_fk_tipo_documento' => 1,
            'i_cedula' => 1012383991,
            'vc_segundo_nombre' => 'Camilo',
            'vc_primer_apellido' => 'Calderón',
            'vc_segundo_apellido' => 'Tapia',
            'vc_genero' => 'M',
            'dt_fecha_nacimiento' => '1992-01-11',
            'i_fk_ciudad' => 2,
            'i_fk_genero' => 5,
            'i_fk_etnia' => 3,
            'i_fk_eps' => 4,
            'vc_rh' => "A+",
            'vc_telefono'=> "3795750 ext 4305",
            'vc_celular'=> "3057467347",
            'vc_direccion'=> "CR 8 15 46",
            'vc_estado' => 1,
        ]);
    }
}
