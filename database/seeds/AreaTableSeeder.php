<?php

use Illuminate\Database\Seeder;

class AreaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
 
        DB::table('tbl_parametros_detalles')->insert([
			[
				'i_pk_id'=>16,
			    'vc_parametro_detalle' => 'AREA DE ALMACEN VoBo',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>17,
			    'vc_parametro_detalle' => 'AREA DE ALMACEN',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>18,
			    'vc_parametro_detalle' => 'AREA DE COMUNICACIONES',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>19,
			    'vc_parametro_detalle' => 'AREA DE CONTABILIDAD',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>20,
			    'vc_parametro_detalle' => 'AREA DE CONTROL INTERNO',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>21,
			    'vc_parametro_detalle' => 'AREA DE CONVOCATORIAS',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>22,
			    'vc_parametro_detalle' => 'AREA DE CORRESPONDENCIA',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>23,
			    'vc_parametro_detalle' => 'AREA DE GESTION DOCUMENTAL VoBo',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>24,
			    'vc_parametro_detalle' => 'AREA DE GESTION DOCUMENTAL',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>25,
			    'vc_parametro_detalle' => 'AREA DE PLANEACION',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>26,
			    'vc_parametro_detalle' => 'AREA DE PRESUPUESTO',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>27,
			    'vc_parametro_detalle' => 'AREA DE PRODUCCION',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>28,
			    'vc_parametro_detalle' => 'AREA DE TECNOLOGÍA VoBo Correo',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>29,
			    'vc_parametro_detalle' => 'AREA DE TECNOLOGÍA',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>30,
			    'vc_parametro_detalle' => 'AREA DE TALENTO HUMANO VoBo Carnet',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>31,
			    'vc_parametro_detalle' => 'AREA DE TALENTO HUMANO VoBo 2',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>32,
			    'vc_parametro_detalle' => 'AREA DE TALENTO HUMANO',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>33,
			    'vc_parametro_detalle' => 'AREA DE TESORERIA',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>34,
			    'vc_parametro_detalle' => 'ATENCION AL CIUDADANO',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>35,
			    'vc_parametro_detalle' => 'ATENCION INTEGRAL A LA PRIMERA INFANCIA',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>36,
			    'vc_parametro_detalle' => 'CINEMATECA DISTRITAL',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>37,
			    'vc_parametro_detalle' => 'DIRECCION GENERAL',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>38,
			    'vc_parametro_detalle' => 'EQUIPAMIENTOS',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>39,
			    'vc_parametro_detalle' => 'ESCENARIO MOVIL',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>40,
			    'vc_parametro_detalle' => 'ESCENARIOS',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>41,
			    'vc_parametro_detalle' => 'GERENCIA DE DANZA',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>42,
			    'vc_parametro_detalle' => 'GERENCIA DE ARTE DRAMATICO',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>43,
			    'vc_parametro_detalle' => 'GERENCIA DE ARTES AUDIOVISUALES',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>44,
			    'vc_parametro_detalle' => 'GERENCIA DE LITERATURA',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>45,
			    'vc_parametro_detalle' => 'GERENCIA DE ARTES PLASTICAS',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>46,
			    'vc_parametro_detalle' => 'GERENCIA DE MUSICA',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>47,
			    'vc_parametro_detalle' => 'GERENCIA DE ESCENARIOS',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>48,
			    'vc_parametro_detalle' => 'JORNADA UNICA',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>49,
			    'vc_parametro_detalle' => 'MEDIA TORTA',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>50,
			    'vc_parametro_detalle' => 'OFICINA ASESORA JURIDICA',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>51,
			    'vc_parametro_detalle' => 'PLANETARIO',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>52,
			    'vc_parametro_detalle' => 'SUBDIRECCION ADMINISTRATIVA Y FINANCIERA',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>53,
			    'vc_parametro_detalle' => 'SUBDIRECCION DE EQUIPAMIENTOS CULTURALES',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>54,
			    'vc_parametro_detalle' => 'SUBDIRECCION DE LAS ARTES',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>55,
			    'vc_parametro_detalle' => 'TEATRO EL PARQUE',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>56,
			    'vc_parametro_detalle' => 'TEATRO JORGE ELIECER GAITAN',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>57,
			    'vc_parametro_detalle' => 'ORFEO VoBo',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>58,
			    'vc_parametro_detalle' => 'ORFEO',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>59,
			    'vc_parametro_detalle' => 'GESTION DOCUMENTAL',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>60,
			    'vc_parametro_detalle' => 'EFORCERS',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
			[
				'i_pk_id'=>61,
			    'vc_parametro_detalle' => 'AREA DE CAJA MENOR',
			    'i_fk_id_parametro' => 6,
			    'i_estado' => 1,
			],
        ]);        

    }
}
