<?php

use Illuminate\Database\Seeder;

class CiudadesParametroDetalleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('tbl_parametros_detalles')->insert([
            [
                'vc_parametro_detalle' => 'MEDELLIN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ABEJORRAL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ABRIAQUI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ALEJANDRIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'AMAGA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'AMALFI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ANDES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ANGELOPOLIS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ANGOSTURA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ANORI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SANTAFE DE ANTIOQUIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ANZA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'APARTADO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ARBOLETES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ARGELIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ARMENIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BARBOSA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BELMIRA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BELLO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BETANIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BETULIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CIUDAD BOLIVAR',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BRICEÑO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BURITICA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CACERES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CAICEDO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CALDAS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CAMPAMENTO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CAÑASGORDAS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CARACOLI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CARAMANTA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CAREPA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL CARMEN DE VIBORAL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CAROLINA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CAUCASIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CHIGORODO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CISNEROS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'COCORNA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CONCEPCION',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CONCORDIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'COPACABANA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'DABEIBA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'DON MATIAS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EBEJICO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL BAGRE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ENTRERRIOS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ENVIGADO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'FREDONIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'FRONTINO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GIRALDO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GIRARDOTA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GOMEZ PLATA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GRANADA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GUADALUPE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GUARNE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GUATAPE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'HELICONIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'HISPANIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ITAGUI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ITUANGO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'JARDIN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'JERICO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA CEJA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA ESTRELLA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA PINTADA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA UNION',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LIBORINA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MACEO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MARINILLA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MONTEBELLO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MURINDO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MUTATA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'NARIÑO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'NECOCLI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'NECHI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'OLAYA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PEÐOL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PEQUE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUEBLORRICO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUERTO BERRIO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUERTO NARE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUERTO TRIUNFO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'REMEDIOS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'RETIRO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'RIONEGRO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SABANALARGA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SABANETA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SALGAR',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN ANDRES DE CUERQUIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN CARLOS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN FRANCISCO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN JERONIMO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN JOSE DE LA MONTAÑA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN JUAN DE URABA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN LUIS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN PEDRO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN PEDRO DE URABA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN RAFAEL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN ROQUE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN VICENTE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SANTA BARBARA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SANTA ROSA DE OSOS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SANTO DOMINGO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL SANTUARIO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SEGOVIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SONSON',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SOPETRAN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TAMESIS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TARAZA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TARSO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TITIRIBI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TOLEDO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TURBO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'URAMITA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'URRAO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VALDIVIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VALPARAISO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VEGACHI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VENECIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VIGIA DEL FUERTE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'YALI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'YARUMAL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'YOLOMBO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'YONDO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ZARAGOZA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BARRANQUILLA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BARANOA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CAMPO DE LA CRUZ',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CANDELARIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GALAPA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'JUAN DE ACOSTA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LURUACO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MALAMBO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MANATI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PALMAR DE VARELA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PIOJO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'POLONUEVO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PONEDERA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUERTO COLOMBIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'REPELON',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SABANAGRANDE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SABANALARGA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SANTA LUCIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SANTO TOMAS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SOLEDAD',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SUAN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TUBARA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'USIACURI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BOGOTA, D.C.',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CARTAGENA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ACHI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ALTOS DEL ROSARIO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ARENAL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ARJONA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ARROYOHONDO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BARRANCO DE LOBA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CALAMAR',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CANTAGALLO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CICUCO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CORDOBA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CLEMENCIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL CARMEN DE BOLIVAR',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL GUAMO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL PEÑON',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'HATILLO DE LOBA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MAGANGUE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MAHATES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MARGARITA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MARIA LA BAJA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MONTECRISTO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MOMPOS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'NOROSI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MORALES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PINILLOS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'REGIDOR',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'RIO VIEJO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN CRISTOBAL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN ESTANISLAO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN FERNANDO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN JACINTO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN JACINTO DEL CAUCA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN JUAN NEPOMUCENO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN MARTIN DE LOBA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN PABLO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SANTA CATALINA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SANTA ROSA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SANTA ROSA DEL SUR',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SIMITI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SOPLAVIENTO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TALAIGUA NUEVO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TIQUISIO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TURBACO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TURBANA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VILLANUEVA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ZAMBRANO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TUNJA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ALMEIDA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'AQUITANIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ARCABUCO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BELEN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BERBEO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BETEITIVA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BOAVITA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BOYACA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BRICEÑO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BUENAVISTA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BUSBANZA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CALDAS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CAMPOHERMOSO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CERINZA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CHINAVITA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CHIQUINQUIRA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CHISCAS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CHITA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CHITARAQUE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CHIVATA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CIENEGA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'COMBITA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'COPER',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CORRALES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'COVARACHIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CUBARA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CUCAITA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CUITIVA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CHIQUIZA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CHIVOR',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'DUITAMA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL COCUY',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL ESPINO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'FIRAVITOBA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'FLORESTA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GACHANTIVA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GAMEZA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GARAGOA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GUACAMAYAS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GUATEQUE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GUAYATA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GsICAN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'IZA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'JENESANO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'JERICO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LABRANZAGRANDE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA CAPILLA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA VICTORIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA UVITA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VILLA DE LEYVA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MACANAL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MARIPI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MIRAFLORES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MONGUA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MONGUI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MONIQUIRA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MOTAVITA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MUZO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'NOBSA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'NUEVO COLON',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'OICATA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'OTANCHE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PACHAVITA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PAEZ',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PAIPA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PAJARITO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PANQUEBA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PAUNA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PAYA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PAZ DE RIO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PESCA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PISBA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUERTO BOYACA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'QUIPAMA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'RAMIRIQUI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'RAQUIRA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'RONDON',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SABOYA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SACHICA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAMACA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN EDUARDO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN JOSE DE PARE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN LUIS DE GACENO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN MATEO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN MIGUEL DE SEMA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN PABLO DE BORBUR',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SANTANA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SANTA MARIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SANTA ROSA DE VITERBO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SANTA SOFIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SATIVANORTE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SATIVASUR',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SIACHOQUE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SOATA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SOCOTA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SOCHA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SOGAMOSO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SOMONDOCO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SORA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SOTAQUIRA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SORACA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SUSACON',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SUTAMARCHAN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SUTATENZA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TASCO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TENZA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TIBANA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TIBASOSA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TINJACA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TIPACOQUE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TOCA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TOGsI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TOPAGA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TOTA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TUNUNGUA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TURMEQUE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TUTA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TUTAZA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'UMBITA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VENTAQUEMADA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VIRACACHA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ZETAQUIRA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MANIZALES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'AGUADAS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ANSERMA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ARANZAZU',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BELALCAZAR',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CHINCHINA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'FILADELFIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA DORADA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA MERCED',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MANZANARES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MARMATO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MARQUETALIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MARULANDA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'NEIRA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'NORCASIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PACORA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PALESTINA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PENSILVANIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'RIOSUCIO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'RISARALDA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SALAMINA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAMANA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN JOSE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SUPIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VICTORIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VILLAMARIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VITERBO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'FLORENCIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ALBANIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BELEN DE LOS ANDAQUIES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CARTAGENA DEL CHAIRA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CURILLO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL DONCELLO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL PAUJIL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA MONTAÑITA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MILAN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MORELIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUERTO RICO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN JOSE DEL FRAGUA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN VICENTE DEL CAGUAN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SOLANO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SOLITA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VALPARAISO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'POPAYAN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ALMAGUER',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ARGELIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BALBOA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BOLIVAR',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BUENOS AIRES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CAJIBIO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CALDONO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CALOTO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CORINTO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL TAMBO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'FLORENCIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GUACHENE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GUAPI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'INZA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'JAMBALO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA SIERRA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA VEGA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LOPEZ',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MERCADERES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MIRANDA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MORALES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PADILLA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PAEZ',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PATIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PIAMONTE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PIENDAMO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUERTO TEJADA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PURACE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ROSAS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN SEBASTIAN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SANTANDER DE QUILICHAO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SANTA ROSA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SILVIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SOTARA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SUAREZ',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SUCRE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TIMBIO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TIMBIQUI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TORIBIO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TOTORO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VILLA RICA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VALLEDUPAR',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'AGUACHICA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'AGUSTIN CODAZZI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ASTREA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BECERRIL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BOSCONIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CHIMICHAGUA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CHIRIGUANA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CURUMANI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL COPEY',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL PASO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GAMARRA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GONZALEZ',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA GLORIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA JAGUA DE IBIRICO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MANAURE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PAILITAS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PELAYA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUEBLO BELLO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'RIO DE ORO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA PAZ',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN ALBERTO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN DIEGO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN MARTIN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TAMALAMEQUE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MONTERIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'AYAPEL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BUENAVISTA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CANALETE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CERETE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CHIMA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CHINU',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CIENAGA DE ORO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'COTORRA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA APARTADA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LORICA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LOS CORDOBAS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MOMIL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MONTELIBANO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MOÑITOS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PLANETA RICA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUEBLO NUEVO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUERTO ESCONDIDO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUERTO LIBERTADOR',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PURISIMA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAHAGUN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN ANDRES SOTAVENTO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN ANTERO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN BERNARDO DEL VIENTO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN CARLOS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN PELAYO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TIERRALTA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VALENCIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'AGUA DE DIOS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ALBAN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ANAPOIMA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ANOLAIMA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ARBELAEZ',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BELTRAN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BITUIMA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BOJACA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CABRERA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CACHIPAY',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CAJICA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CAPARRAPI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CAQUEZA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CARMEN DE CARUPA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CHAGUANI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CHIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CHIPAQUE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CHOACHI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CHOCONTA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'COGUA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'COTA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CUCUNUBA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL COLEGIO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL PEÑON',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL ROSAL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'FACATATIVA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'FOMEQUE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'FOSCA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'FUNZA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'FUQUENE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'FUSAGASUGA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GACHALA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GACHANCIPA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GACHETA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GAMA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GIRARDOT',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GRANADA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GUACHETA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GUADUAS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GUASCA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GUATAQUI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GUATAVITA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GUAYABAL DE SIQUIMA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GUAYABETAL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GUTIERREZ',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'JERUSALEN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'JUNIN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA CALERA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA MESA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA PALMA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA PEÑA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA VEGA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LENGUAZAQUE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MACHETA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MADRID',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MANTA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MEDINA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MOSQUERA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'NARIÑO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'NEMOCON',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'NILO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'NIMAIMA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'NOCAIMA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VENECIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PACHO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PAIME',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PANDI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PARATEBUENO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PASCA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUERTO SALGAR',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PULI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'QUEBRADANEGRA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'QUETAME',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'QUIPILE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'APULO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'RICAURTE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN ANTONIO DEL TEQUENDAMA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN BERNARDO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN CAYETANO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN FRANCISCO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN JUAN DE RIO SECO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SASAIMA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SESQUILE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SIBATE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SILVANIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SIMIJACA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SOACHA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SOPO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SUBACHOQUE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SUESCA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SUPATA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SUSA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SUTATAUSA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TABIO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TAUSA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TENA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TENJO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TIBACUY',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TIBIRITA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TOCAIMA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TOCANCIPA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TOPAIPI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'UBALA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'UBAQUE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VILLA DE SAN DIEGO DE UBATE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'UNE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'UTICA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VERGARA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VIANI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VILLAGOMEZ',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VILLAPINZON',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VILLETA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VIOTA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'YACOPI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ZIPACON',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ZIPAQUIRA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'QUIBDO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ACANDI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ALTO BAUDO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ATRATO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BAGADO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BAHIA SOLANO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BAJO BAUDO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BOJAYA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL CANTON DEL SAN PABLO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CARMEN DEL DARIEN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CERTEGUI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CONDOTO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL CARMEN DE ATRATO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL LITORAL DEL SAN JUAN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ISTMINA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'JURADO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LLORO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MEDIO ATRATO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MEDIO BAUDO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MEDIO SAN JUAN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'NOVITA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'NUQUI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'RIO IRO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'RIO QUITO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'RIOSUCIO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN JOSE DEL PALMAR',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SIPI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TADO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'UNGUIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'UNION PANAMERICANA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'NEIVA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ACEVEDO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'AGRADO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'AIPE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ALGECIRAS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ALTAMIRA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BARAYA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CAMPOALEGRE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'COLOMBIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ELIAS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GARZON',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GIGANTE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GUADALUPE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'HOBO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'IQUIRA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ISNOS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA ARGENTINA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA PLATA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'NATAGA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'OPORAPA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PAICOL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PALERMO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PALESTINA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PITAL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PITALITO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'RIVERA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SALADOBLANCO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN AGUSTIN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SANTA MARIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SUAZA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TARQUI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TESALIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TELLO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TERUEL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TIMANA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VILLAVIEJA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'YAGUARA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'RIOHACHA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ALBANIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BARRANCAS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'DIBULLA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'DISTRACCION',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL MOLINO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'FONSECA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'HATONUEVO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA JAGUA DEL PILAR',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MAICAO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MANAURE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN JUAN DEL CESAR',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'URIBIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'URUMITA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VILLANUEVA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SANTA MARTA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ALGARROBO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ARACATACA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ARIGUANI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CERRO SAN ANTONIO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CHIBOLO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CIENAGA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CONCORDIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL BANCO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL PIÑON',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL RETEN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'FUNDACION',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GUAMAL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'NUEVA GRANADA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PEDRAZA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PIJIÑO DEL CARMEN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PIVIJAY',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PLATO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUEBLOVIEJO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'REMOLINO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SABANAS DE SAN ANGEL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SALAMINA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN SEBASTIAN DE BUENAVISTA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN ZENON',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SANTA ANA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SANTA BARBARA DE PINTO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SITIONUEVO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TENERIFE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ZAPAYAN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ZONA BANANERA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VILLAVICENCIO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ACACIAS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BARRANCA DE UPIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CABUYARO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CASTILLA LA NUEVA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CUBARRAL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CUMARAL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL CALVARIO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL CASTILLO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL DORADO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'FUENTE DE ORO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GRANADA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GUAMAL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MAPIRIPAN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MESETAS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA MACARENA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'URIBE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LEJANIAS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUERTO CONCORDIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUERTO GAITAN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUERTO LOPEZ',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUERTO LLERAS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUERTO RICO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'RESTREPO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN CARLOS DE GUAROA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN JUAN DE ARAMA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN JUANITO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN MARTIN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VISTAHERMOSA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PASTO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ALBAN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ALDANA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ANCUYA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ARBOLEDA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BARBACOAS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BELEN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BUESACO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'COLON',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CONSACA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CONTADERO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CORDOBA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CUASPUD',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CUMBAL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CUMBITARA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CHACHAGsI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL CHARCO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL PEÑOL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL ROSARIO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL TABLON DE GOMEZ',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL TAMBO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'FUNES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GUACHUCAL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GUAITARILLA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GUALMATAN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ILES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'IMUES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'IPIALES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA CRUZ',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA FLORIDA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA LLANADA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA TOLA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA UNION',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LEIVA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LINARES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LOS ANDES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MAGsI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MALLAMA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MOSQUERA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'NARIÑO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'OLAYA HERRERA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'OSPINA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'FRANCISCO PIZARRO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'POLICARPA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'POTOSI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PROVIDENCIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUERRES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUPIALES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'RICAURTE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ROBERTO PAYAN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAMANIEGO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SANDONA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN BERNARDO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN LORENZO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN PABLO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN PEDRO DE CARTAGO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SANTA BARBARA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SANTACRUZ',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAPUYES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TAMINANGO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TANGUA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN ANDRES DE TUMACO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TUQUERRES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'YACUANQUER',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CUCUTA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ABREGO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ARBOLEDAS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BOCHALEMA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BUCARASICA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CACOTA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CACHIRA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CHINACOTA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CHITAGA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CONVENCION',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CUCUTILLA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'DURANIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL CARMEN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL TARRA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL ZULIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GRAMALOTE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'HACARI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'HERRAN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LABATECA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA ESPERANZA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA PLAYA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LOS PATIOS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LOURDES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MUTISCUA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'OCAÑA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PAMPLONA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PAMPLONITA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUERTO SANTANDER',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'RAGONVALIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SALAZAR',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN CALIXTO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN CAYETANO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SANTIAGO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SARDINATA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SILOS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TEORAMA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TIBU',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TOLEDO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VILLA CARO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VILLA DEL ROSARIO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ARMENIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BUENAVISTA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CALARCA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CIRCASIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CORDOBA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'FILANDIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GENOVA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA TEBAIDA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MONTENEGRO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PIJAO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'QUIMBAYA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SALENTO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PEREIRA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'APIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BALBOA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BELEN DE UMBRIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'DOSQUEBRADAS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GUATICA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA CELIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA VIRGINIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MARSELLA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MISTRATO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUEBLO RICO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'QUINCHIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SANTA ROSA DE CABAL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SANTUARIO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BUCARAMANGA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'AGUADA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ALBANIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ARATOCA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BARBOSA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BARICHARA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BARRANCABERMEJA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BETULIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BOLIVAR',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CABRERA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CALIFORNIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CAPITANEJO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CARCASI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CEPITA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CERRITO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CHARALA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CHARTA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CHIMA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CHIPATA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CIMITARRA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CONCEPCION',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CONFINES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CONTRATACION',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'COROMORO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CURITI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL CARMEN DE CHUCURI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL GUACAMAYO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL PEÑON',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL PLAYON',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ENCINO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ENCISO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'FLORIAN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'FLORIDABLANCA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GALAN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GAMBITA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GIRON',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GUACA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GUADALUPE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GUAPOTA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GUAVATA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GsEPSA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'HATO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'JESUS MARIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'JORDAN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA BELLEZA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LANDAZURI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA PAZ',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LEBRIJA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LOS SANTOS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MACARAVITA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MALAGA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MATANZA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MOGOTES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MOLAGAVITA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'OCAMONTE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'OIBA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ONZAGA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PALMAR',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PALMAS DEL SOCORRO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PARAMO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PIEDECUESTA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PINCHOTE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUENTE NACIONAL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUERTO PARRA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUERTO WILCHES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'RIONEGRO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SABANA DE TORRES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN ANDRES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN BENITO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN GIL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN JOAQUIN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN JOSE DE MIRANDA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN MIGUEL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN VICENTE DE CHUCURI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SANTA BARBARA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SANTA HELENA DEL OPON',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SIMACOTA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SOCORRO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SUAITA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SUCRE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SURATA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TONA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VALLE DE SAN JOSE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VELEZ',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VETAS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VILLANUEVA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ZAPATOCA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SINCELEJO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BUENAVISTA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CAIMITO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'COLOSO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'COROZAL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'COVEÑAS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CHALAN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL ROBLE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GALERAS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GUARANDA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA UNION',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LOS PALMITOS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MAJAGUAL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MORROA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'OVEJAS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PALMITO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAMPUES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN BENITO ABAD',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN JUAN DE BETULIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN MARCOS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN ONOFRE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN PEDRO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN LUIS DE SINCE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SUCRE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SANTIAGO DE TOLU',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TOLU VIEJO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'IBAGUE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ALPUJARRA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ALVARADO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'AMBALEMA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ANZOATEGUI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ARMERO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ATACO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CAJAMARCA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CARMEN DE APICALA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CASABIANCA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CHAPARRAL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'COELLO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'COYAIMA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CUNDAY',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'DOLORES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ESPINAL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'FALAN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'FLANDES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'FRESNO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GUAMO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'HERVEO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'HONDA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ICONONZO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LERIDA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LIBANO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MARIQUITA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MELGAR',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MURILLO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'NATAGAIMA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ORTEGA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PALOCABILDO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PIEDRAS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PLANADAS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PRADO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PURIFICACION',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'RIOBLANCO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'RONCESVALLES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ROVIRA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SALDAÑA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN ANTONIO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN LUIS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SANTA ISABEL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SUAREZ',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VALLE DE SAN JUAN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VENADILLO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VILLAHERMOSA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VILLARRICA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CALI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ALCALA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ANDALUCIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ANSERMANUEVO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ARGELIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BOLIVAR',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BUENAVENTURA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GUADALAJARA DE BUGA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BUGALAGRANDE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CAICEDONIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CALIMA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CANDELARIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CARTAGO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'DAGUA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL AGUILA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL CAIRO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL CERRITO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL DOVIO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'FLORIDA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GINEBRA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'GUACARI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'JAMUNDI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA CUMBRE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA UNION',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA VICTORIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'OBANDO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PALMIRA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PRADERA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'RESTREPO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'RIOFRIO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ROLDANILLO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN PEDRO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SEVILLA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TORO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TRUJILLO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TULUA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ULLOA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VERSALLES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VIJES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'YOTOCO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'YUMBO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ZARZAL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ARAUCA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ARAUQUITA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CRAVO NORTE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'FORTUL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUERTO RONDON',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SARAVENA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TAME',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'YOPAL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'AGUAZUL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CHAMEZA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'HATO COROZAL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA SALINA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MANI',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MONTERREY',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'NUNCHIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'OROCUE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PAZ DE ARIPORO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PORE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'RECETOR',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SABANALARGA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SACAMA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN LUIS DE PALENQUE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TAMARA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TAURAMENA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TRINIDAD',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VILLANUEVA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MOCOA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'COLON',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'ORITO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUERTO ASIS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUERTO CAICEDO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUERTO GUZMAN',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LEGUIZAMO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SIBUNDOY',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN FRANCISCO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN MIGUEL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SANTIAGO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VALLE DEL GUAMUEZ',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'VILLAGARZON',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN ANDRES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PROVIDENCIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LETICIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL ENCANTO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA CHORRERA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA PEDRERA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA VICTORIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MIRITI - PARANA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUERTO ALEGRIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUERTO ARICA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUERTO NARIÑO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUERTO SANTANDER',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TARAPACA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'INIRIDA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'BARRANCO MINAS',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MAPIRIPANA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN FELIPE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUERTO COLOMBIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA GUADALUPE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CACAHUAL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PANA PANA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MORICHAL',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SAN JOSE DEL GUAVIARE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CALAMAR',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'EL RETORNO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MIRAFLORES',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'MITU',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CARURU',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PACOA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'TARAIRA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PAPUNAUA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'YAVARATE',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'PUERTO CARREÑO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'LA PRIMAVERA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'SANTA ROSALIA',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],
            [
                'vc_parametro_detalle' => 'CUMARIBO',
                'i_fk_id_parametro' => 2,
                'i_estado' => 1,
            ],

		]);
    }
}
