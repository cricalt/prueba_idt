<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAprobacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_aprobacion', function (Blueprint $table) {
            $table->increments('i_pk_id');
            $table->integer('i_fk_id_padre')->unsigned()->nullable();
            $table->foreign('i_fk_id_padre')->references('i_pk_id')->on('tbl_aprobacion')->nullable(); 
            $table->integer('i_fk_id_area')->unsigned()->nullable(); //join parametro_detalle
            $table->integer('i_tipo_persona');
            $table->integer('i_orden')->nullable();
            $table->string('vc_numeral')->nullable();
            $table->tinyInteger('i_estado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_aprobacion', function (Blueprint $table)
        {
            $table->dropForeign('tbl_aprobacion_i_fk_id_padre_foreign');
        });        
        Schema::dropIfExists('tbl_aprobacion');
    }
}
