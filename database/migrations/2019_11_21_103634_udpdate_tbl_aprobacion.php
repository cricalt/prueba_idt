<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UdpdateTblAprobacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('tbl_aprobacion', function (Blueprint $table) {

            $table->renameColumn('i_fk_id_area', 'i_fk_id_area_pys');
            $table->integer('i_fk_id_area_')->unsigned()->nullable();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('tbl_aprobacion', function (Blueprint $table)
        {
            $table->renameColumn('i_fk_id_area_pys', 'i_fk_id_area');
            $table->dropColumn('i_fk_id_area_');
            
            
        });        
        
    }
}
