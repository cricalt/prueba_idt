<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContratoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_contrato', function (Blueprint $table) {
            $table->increments('i_pk_id');
            $table->integer('i_fk_id_usuario')->unsigned();  //join con usuario              
            $table->integer('i_fk_id_area')->unsigned();
            $table->integer('i_contrato');
            $table->year('y_anio');
            $table->date('d_fecha_inicial');
            $table->date('d_fecha_final');
            $table->tinyInteger('i_cesion');
            $table->integer('i_fk_id_cedente')->unsigned()->nullable(); //join con usuario                     
            $table->tinyInteger('i_prorroga');
            $table->date('d_fecha_final_prorroga')->nullable();
            $table->tinyInteger('i_terminacion_anticipadas');
            $table->date('d_fecha_terminacion')->nullable();
            $table->integer('i_fk_id_supervisor')->unsigned();             
            $table->timestamps(); 
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_contrato');
    }
}
