<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldSolicitudAprobacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_solicitud_aprobacion', function (Blueprint $table) {
            $table->integer('i_carnet')->nullable();
            $table->text('tx_observacion_carnet')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_solicitud_aprobacion', function (Blueprint $table)
        {
             $table->dropColumn('i_carnet');
             $table->dropColumn('tx_observacion_carnet');
        });
    }
}
