<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitudTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_solicitud', function (Blueprint $table) {
            $table->increments('i_pk_id');
            $table->foreign('i_pk_id')->references('i_pk_id')->on('tbl_contrato');
            $table->timestamp('d_fecha_creacion')->useCurrent();
            $table->tinyInteger('i_estado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_solicitud', function (Blueprint $table)
        {
            $table->dropForeign('tbl_solicitud_i_pk_id_foreign');
        });
                 
        Schema::dropIfExists('tbl_solicitud');
    }
}
