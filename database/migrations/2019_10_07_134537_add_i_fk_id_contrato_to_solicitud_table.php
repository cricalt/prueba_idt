<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIFkIdContratoToSolicitudTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_solicitud', function (Blueprint $table)
        {
            $table->integer('i_fk_id_contrato')->unsigned(); 
            $table->foreign('i_fk_id_contrato')->references('i_pk_id')->on('tbl_contrato'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_solicitud', function (Blueprint $table)
        {
            $table->dropForeign('tbl_solicitud_i_fk_id_contrato_foreign');
            $table->dropColumn('i_fk_id_contrato');
        });
    }
}
