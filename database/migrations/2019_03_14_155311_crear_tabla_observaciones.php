<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaObservaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_observacion', function (Blueprint $table) {
            $table->increments('i_pk_id');
            $table->integer('i_fk_id_solicitud')->unsigned()->nullable();
            $table->foreign('i_fk_id_solicitud')->references('i_pk_id')->on('tbl_solicitud')->nullable(); 
            $table->text('tx_observacion')->nullable(); 
            $table->integer('i_fk_id_usuario');
            $table->integer('i_fk_id_area');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_observacion', function (Blueprint $table)
        {
            $table->dropForeign('tbl_observacion_i_fk_id_solicitud_foreign');
        });
                 
        Schema::dropIfExists('tbl_observacion');
    }
}
