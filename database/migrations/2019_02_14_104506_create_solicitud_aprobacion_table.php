<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitudAprobacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_solicitud_aprobacion', function (Blueprint $table) {
            $table->increments('i_pk_id');
            $table->integer('i_fk_id_solicitud')->unsigned();
            $table->foreign('i_fk_id_solicitud')->references('i_pk_id')->on('tbl_solicitud');
            $table->integer('i_fk_id_aprobacion')->unsigned();
            $table->foreign('i_fk_id_aprobacion')->references('i_pk_id')->on('tbl_aprobacion');
            $table->integer('i_fk_id_usuario')->unsigned();
            $table->timestamp('d_fecha_creacion')->nullable(); 
            $table->text('tx_observacion')->nullable(); 
            $table->tinyInteger('i_estado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void 
     */
    public function down()
    {
        Schema::table('tbl_solicitud_aprobacion', function (Blueprint $table)
        {
            $table->dropForeign('tbl_solicitud_aprobacion_i_fk_id_solicitud_foreign');
            $table->dropForeign('tbl_solicitud_aprobacion_i_fk_id_aprobacion_foreign');
        });      
          
        Schema::dropIfExists('tbl_solicitud_aprobacion');
    }
}
