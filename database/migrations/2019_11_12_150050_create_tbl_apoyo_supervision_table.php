<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblApoyoSupervisionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_apoyo_supervision', function (Blueprint $table) {
            $table->increments('i_pk_id');
            $table->integer('i_fk_id_supervisor')->unsigned();  //join con usuario       
            $table->integer('i_fk_id_apoyo')->unsigned();  //join con usuario 
            $table->tinyInteger('i_estado');
            $table->timestamps();
        });     
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_apoyo_supervision');
    }
}
