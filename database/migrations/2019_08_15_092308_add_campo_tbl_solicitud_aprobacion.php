<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCampoTblSolicitudAprobacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::table('tbl_solicitud_aprobacion', function (Blueprint $table) {
            $table->integer('i_secop1');
            $table->integer('i_secop2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_solicitud_aprobacion', function (Blueprint $table)
        {
             $table->dropColumn('i_secop1');
             $table->dropColumn('i_secop2');
        });
    }
}
