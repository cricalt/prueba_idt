<?php

return array( 
  'conexionpys' => 'mysql', 
  'conexionadm' => 'baseadmin',  
  'contrato' => 'App\Modulos\Contrato\Contrato',
  'parametrodetalle' => 'App\Modulos\Parametrizable\ParametroDetalles', 
  'aprobacion' => 'App\Modulos\Solicitudes\Aprobacion', 
  'solicitud' => 'App\Modulos\Solicitudes\Solicitud',  
  'solicitudaprobacion' => 'App\Modulos\Solicitudes\SolicitudAprobacion',
  'modelo_observacion' => 'App\Modulos\Observacion\Observacion',
  'usuario' => 'App\Modulos\Usuario\User',
   
  'modulo' => '', 
  'seccion' => 'Personas', 
  'prefijo_ruta' => 'personas', 
  'prefijo_ruta_modulo' => 'actividad', 
 
  'modelo_user' => 'App\Modulos\Usuario\User',
  'modelo_tipo' => 'idartes\usuario\Tipo', 
  'modelo_darametrodetalles' => 'idartes\usuario\ParametroDetalles', 
  'modelo_actividad' => 'idartes\usuario\Actividad',  
  'modelo_modulo' => 'idartes\usuario\Modulo', 
  
   
  //vistas que carga las vistas 
  'vista_lista' => 'list', 
 
  //lista 
  'lista'  => 'idrd.usuarios.lista',   

);