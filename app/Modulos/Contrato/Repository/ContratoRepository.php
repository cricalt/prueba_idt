<?php

namespace App\Modulos\Contrato\Repository;
use App\Modulos\Contrato\Contrato;
use idartes\usuario\Repository\UsuarioInterface;

class ContratoRepository{

    protected $usuarioRepository;

    public function __construct(UsuarioInterface $usuarioRepository){
        $this->usuarioRepository=$usuarioRepository;
    }

	public function obtenerContratoPorNumeroYAnio($ncontrato,$anio){
		//dd($request->i_contrato.'-'.$request->y_anio);
		return Contrato::where('i_contrato','=',$ncontrato)
                       ->where('y_anio','=',$anio)->first();
	}
	public function obtenerContratoPorId($id){
		return Contrato::with('usuario')->find($id); 
	}
	public function obtenerContratoPorUsuario($idUsuario){
		return Contrato::where('i_fk_id_usuario','=',$idUsuario)
						->where('i_estado',1)
						->get(); 
	}	
	public function obtenerContratoPorAreaYAnio($area,$anio){
		return Contrato::where('i_fk_id_area', '=', $area)
		            ->where('y_anio','=',$anio)
		            ->with('usuario')
		            ->with('cedente')
		            ->with('area')
		            ->get(); 		
	}


	public function obtenerContratoPorFecha($fecha_inicio,$fecha_fin){
		return Contrato::whereBetween('d_fecha_final', [$fecha_inicio, $fecha_fin])->orderBy('d_fecha_final', 'desc')->get(); 		
	}

	public function validarYCrearContrato($request){
		$contrato = $this->obtenerContratoPorUsuario($request->i_fk_id_usuario);
		 if($contrato->count()>0){
		 	return [
                'message'=>'El usuario ya tiene un contrato activo asociado',
                'title'=>'Operación fallida!',
                'type'=>'error'
            ];
		 }
		 
		 $contrato = $this->obtenerContratoPorNumeroYAnio($request->i_contrato,$request->y_anio);
		 if($contrato!=null){
		 	return [
                'message'=>'Ya existe el contrato '.$request->i_contrato.' - '.$request->y_anio,
                'title'=>'Operación fallida!',
                'type'=>'error'
            ];
		 }

		if(isset($request->i_cesion)){
		 	return [
                'message'=>'La cesión de contrato se debe realizar por la opción de modificación',
                'title'=>'Operación fallida!',
                'type'=>'error'
            ];
		 }

		 if($this->crear($request)){ 
		 	$this->actualizarFechaFinContrato($request, $request->i_fk_id_usuario); 
		 	return [
                'message'=>'Se ha creado el contrato con los datos ingresados',
                'title'=>'Registro Creado!',
                'type'=>'success'
            ];
		 }else{
		 	return [
                'message'=>'No ha sido posible crear el registro de contrato',
                'title'=>'Operación fallida!',
                'type'=>'error'
            ];
		 } 


	}
	public function crear($request){
		$contrato = new Contrato();
		$data = $request->only($contrato->getFillable());
		$data['i_cesion'] = (isset($request->i_cesion)) ? 1 : 0;
		$data['i_prorroga'] = (isset($request->i_prorroga)) ? 1 : 0;
		$data['i_terminacion_anticipadas'] = (isset($request->i_i_terminacion_anticipadas)) ? 1 : 0;
		$data['i_estado'] = 1;   
		try{
			return $contrato->fill($data)->save();
		}catch(\Exception $e){
			echo $e->getMessage(); 
			return 0;
		}
	}


	public function validarYEditarContrato($request,$id){
		$contrato = Contrato::find($id);
		//dd($contrato->i_estado);
		if($contrato->i_estado==0){
		 	return [
                'message'=>'El contrato se encuentra inactivo',
                'title'=>'Operación fallida!',
                'type'=>'error'
            ];			
		}  
		$contrato = $this->obtenerContratoPorUsuario($request->i_fk_id_cedente);
		if(isset($request->i_fk_id_cedente) && $contrato->count()>0){
		 	return [
                'message'=>'El contratista cedente ya tiene un contrato activo',
                'title'=>'Operación fallida!',
                'type'=>'error'
            ];	
		}

 		if($this->editar($request,$contrato)){
 			$this->actualizarFechaFinContrato($request, $request->i_fk_id_usuario); 
		 	return [
                'message'=>'Se modificado el contrato con éxito',
                'title'=>'Registro Modificado!',
                'type'=>'success'
            ];
		 }else{
		 	return [
                'message'=>'No ha sido posible editar el contrato',
                'title'=>'Operación fallida!',
                'type'=>'error'
            ];
		 } 


		/*if(isset($request->i_cesion)){
		 	return [
                'message'=>'La cesión de contrato se debe realizar por la opción de modificación',
                'title'=>'Operación fallida!',
                'type'=>'error'
            ];
		}*/		

	}

	public function editar($request,$contrato){
		//dd($request);
		$data = $request->only($contrato->getFillable());
		$data['i_prorroga'] = (isset($request->i_prorroga)) ? 1 : 0;
		$data['i_terminacion_anticipadas'] = (isset($request->i_terminacion_anticipadas)) ? 1 : 0;	
		$data['i_estado'] = (isset($request->i_cesion)) ? 0 : 1; //inactiva el contrato en caso de que haya cesión
		//dd($data);
		try{
			
			$modifica = true;
			if(isset($request->i_cesion)){ 
				$data2=$data;
				$data2['i_fk_id_usuario']=$data2['i_fk_id_cedente'];
				$data2['d_fecha_inicial']=$data2['d_fecha_cesion'];
				$data2['i_estado']=1;
				$data2['i_cesion']=0;
				unset($data2['i_fk_id_cedente']);
				unset($data2['d_fecha_cesion']);						
				//dd($data2);
				$contrato2= new Contrato();
				$modifica=$contrato2->fill($data2)->save();
			}

			if($modifica){
				return $contrato->fill($data)->save();
			}
			else{
				return false; //No editó el contrato
			} 				
		}catch(\Exception $e){
			echo $e->getMessage(); 
			return 0;
		}		
	}

	public function actualizarFechaFinContrato($request, $id){
		$d_fecha_fin_contrato="";	

		$d_fecha_fin_contrato = ($request->d_fecha_final!=null) ? $request->d_fecha_final : null ;
		$d_fecha_fin_contrato = ($request->d_fecha_final_prorroga!=null) ? $request->d_fecha_final_prorroga : $d_fecha_fin_contrato ;
		$d_fecha_fin_contrato = ($request->d_fecha_terminacion!=null) ? $request->d_fecha_terminacion : $d_fecha_fin_contrato; 
		$d_fecha_fin_contrato = ($request->d_fecha_cesion!=null) ? $request->d_fecha_cesion : $d_fecha_fin_contrato;  
		$userRequest = $request;  
		$userRequest->request->add(['d_fecha_fin_contrato' => $d_fecha_fin_contrato]);
		$this->usuarioRepository->actualizar($userRequest,$id); 
	}
}