<?php

namespace App\Modulos\Contrato;

use Illuminate\Database\Eloquent\Model;


class Contrato extends Model
{
    /**
     * The name of the table.
     *
     * @var string
     */
    protected $table = 'tbl_contrato';

    /**
     * The name of the primary key.
     *
     * @var string
     */
    protected $primaryKey= 'i_pk_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'i_fk_id_usuario', 
        'i_fk_id_area', 
        'i_contrato', 
        'y_anio', 
        'd_fecha_inicial', 
        'd_fecha_final', 
        'i_cesion', 
        'i_fk_id_cedente', 
        'i_prorroga', 
        'd_fecha_final_prorroga', 
        'i_terminacion_anticipadas', 
        'd_fecha_terminacion', 
        'i_fk_id_supervisor',
        'i_estado',
        'd_fecha_cesion',
        'i_fk_id_sub_area'
    ]; 

    protected $casts = [ 
        'i_cesion' => 'boolean',
        'i_prorroga' => 'boolean',
        'i_terminacion_anticipadas' => 'boolean' 
    ];

    protected $appends = ['fecha_final'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public $timestamps = true;

    /**
     * Set the town's name to upper.
     *
     * @param  string $value
     * @return void
     */

     public function __construct()
    {
        //$this->connection = config('modelos.conexionpys');
    } 

    public function solicitud()
    {
       return  $this->hasMany(config('modelos.solicitud'), 'i_fk_id_contrato', 'i_pk_id');      
    } 

    public function area()
    {       
        return $this->belongsTo(config('modelos.parametrodetalle'), 'i_fk_id_area', 'i_pk_id');
    } 

    public function usuario()
    {
         return $this->belongsTo(config('usuarios.modelo_user'), 'i_fk_id_usuario', 'id');
    }


    public function cedente()
    {
         return $this->belongsTo(config('usuarios.modelo_user'), 'i_fk_id_cedente', 'id');
    } 

    public function supervisor()
    {
         return $this->belongsTo(config('usuarios.modelo_user'), 'i_fk_id_supervisor', 'id');
    } 

    public function getFechaFinalAttribute(){
        $fecha_final = ($this->d_fecha_final_prorroga !=null &&  
                        $this->d_fecha_final_prorroga > $this->d_fecha_final) ? $this->d_fecha_final_prorroga :  $this->d_fecha_final;        
        $fecha_final = ($this->d_fecha_terminacion !=null &&  
                        $this->d_fecha_terminacion < $fecha_final) ? $this->d_fecha_terminacion :  $fecha_final;
        $fecha_final = ($this->d_fecha_cesion !=null &&  
                        $this->d_fecha_cesion < $fecha_final) ? $this->d_fecha_cesion :  $fecha_final; 
        return  $fecha_final;       
    }

    public function sub_area()
    {       
        return $this->belongsTo(config('modelos.parametrodetalle'), 'i_fk_id_sub_area', 'i_pk_id');
    }     


    
}