<?php

namespace App\Modulos\Contrato\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modulos\Contrato\Contrato;
use App\Modulos\Configuracion\Configuracion;
use App\Modulos\Usuario\User;
use App\Modulos\Contrato\Repository\ContratoRepository;
use App\Modulos\Estado\Estado;
use App\Modulos\Solicitudes\Requests\SolicitudesHistoriaRequest;

class ContratosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $contratoRepository;
    protected $usuarioRepository;
    protected $parametrosRepository;

    public function __construct(ContratoRepository $contratoRepository,Configuracion $configuracion ){
        $this->contratoRepository=$contratoRepository;
        $this->parametrosRepository=$configuracion;
    }
    public function index()
    {
        return redirect('/gestionar-contratos'); 
    } 


    public function inicio(){
        $id_area = auth()->user()->i_fk_area;
        $id_permiso = auth()->user()->id;

        $especifico  = $this->parametrosRepository->obtenerParametroEspecifico($id_area);
        $general = $this->parametrosRepository->obtenerParametro(6);

        $data = [
            'areas'=>($id_permiso==61)? $general:$especifico,
            'anios'=>$this->parametrosRepository->obtenerAnios(),
            'usuarios'=>$this->parametrosRepository->obtenerUsuarios(),
            'sub_areas'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_SUB_AREAS),
        ];      
        //dd($data);
        return view('material.sections.contrato.gestionar-contratos',$data);     
        
    }    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $mensajes = $this->contratoRepository->validarYCrearContrato($request);
        if($mensajes['type']=='success') 
            return redirect('/gestionar-contratos')->with($mensajes);
        else
            return redirect('/gestionar-contratos')->with($mensajes)->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contrato = $this->contratoRepository->obtenerContratoPorId($id); 
        $data = [ 
            'areas'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_AREAS),
            'tipoDocumentos'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_TIPO_DOCUMENTO),
            'ciudades'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_CIUDAD),
            'generos'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_GENERO),
            'etnias'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_ETNIA),
            'eps'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_EPS),
            'anios'=>$this->parametrosRepository->obtenerAnios(),
            'usuarios'=>$this->parametrosRepository->obtenerUsuarios(),
            'sub_areas'=>$this->parametrosRepository->obtenerParametro(Estado::CODIGO_SUB_AREAS),
            
            'contrato'=>$contrato
        ]; 
        //$returnHTML="";        
        $returnHTML = view('material.sections.contrato.form.edit',$data)->render();
        return response()->json(array('success' => true, 'html'=>$returnHTML,'contrato'=>$contrato)); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mensajes = $this->contratoRepository->validarYEditarContrato($request,$id);
        if($mensajes['type']=='success') 
            return redirect('/gestionar-contratos')->with($mensajes);    
        else
            return redirect('/gestionar-contratos')->with($mensajes)->withInput();                
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function consultarContratosArea(Request $request){    
        $area=intval($request['i_fk_id_area']);
        $anio=intval($request['y_anio']); 
        $c= $this->contratoRepository->obtenerContratoPorAreaYAnio($area,$anio);
        //return response()->json($c); 
        $returnHTML = view('material.sections.contrato.tabla-contratos')->with('contratos', $c)->render();
        return response()->json(array('success' => true, 'html'=>$returnHTML)); 
    } 


    public function busqueda_proximas_solicitudes(Request $request)
    {

        $id = auth()->user()->id;
        $fecha_inicio=$request['fecha_inicio'];
        $fecha_fin=$request['fecha_fin']; 

        $contratos= $this->contratoRepository->obtenerContratoPorFecha($fecha_inicio,$fecha_fin);

        $dataUser = User::with('tiposPersona')->find($id);

        $data = [
            'solicitudes_data'=>$contratos,
            'tipoUser'=>$dataUser->tiposPersona->where('i_fk_id_modulo',2)->pluck('vc_tipo', 'i_pk_id')->toArray(),
            'nombrePerfil'=>Configuracion::tipoUsuario($request['tipoUsuario']),
            'tipoUsuario'=>$request['tipoUsuario'],
        ];
        
        return view('material.sections.solicitudes.proximas_solicitudes',$data);
    }

}
