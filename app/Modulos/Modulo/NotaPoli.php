<?php

namespace App\Modulos\Modulo;

use Illuminate\Database\Eloquent\Model;

class NotaPoli extends Model
{
    protected $table = 'tbl_notas_poli';
    protected $primaryKey= 'i_pk_id';
    protected $fillable = [
        'i_fk_id_estudiante', 'i_fk_id_curso','d_nota_escenario_1','d_nota_escenario_2','d_nota_escenario_3','i_estado'
    ];
    public $timestamps = true;

    public $appends = ['promedio'];

    public function curso()
    {
    	return $this->belongsTo(config('usuarios.modelo_darametrodetalles'), 'i_fk_id_curso','i_pk_id');
    }  
    
    public function estudiante()
    {
    	return $this->belongsTo(config('usuarios.modelo_user'), 'i_fk_id_estudiante','id');
    }

    public function getPromedioAttribute(){
        return round(($this->d_nota_escenario_1+$this->d_nota_escenario_2+$this->d_nota_escenario_3)/3,2);
    }
}