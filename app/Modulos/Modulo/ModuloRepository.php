<?php

namespace App\Modulos\Modulo;

use App\Modulos\Modulo\Modulo;
use App\Modulos\Modulo\ModuloInterface;
use Illuminate\Support\Facades\Cache;
use App\Modulos\Configuracion\Configuracion;
use Yajra\DataTables\DataTables;
use App\Modulos\Modulo\NotaPoli;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ModuloRepository implements ModuloInterface
{
	public function __construct(){}

	public function crear(array $data)
	{
		$modulo = new Modulo;
		return $this->procesar($modulo, $data);
	}

    public function dataTable(array $relaciones=[])
    {

            foreach (Modulo::cursor() as $modulo) {
                $actions = '<a href="javascript:;" data-id="'.$modulo->i_pk_id.'" data-nombre="'.$modulo->vc_modulo.'" data-estado="'.$modulo->i_estado.'"  data-direccion="'.$modulo->vc_redireccion.'" data-imagen="'.$modulo->vc_imagen.'"  data-to="false" class="btn btn-simple btn-warning btn-icon edit"><i class="icon-pencil"></i></a>';
                $table[] = [
                    'i_pk_id'           => $modulo->i_pk_id,
                    'vc_nombre'         => $modulo->vc_modulo,
                    'vc_redireccion'    => $modulo->vc_redireccion,
                    'vc_imagen'         => $modulo->vc_imagen,
                    'vc_estado'         => Configuracion::estado($modulo->i_estado),
                    'link'              => $actions
                ];
                $actions = null;
            }

        return Datatables::of($table)->addIndexColumn()->rawColumns(['link'])->make(true);
	}

	public function actualizar(array $request, $id)
	{
        $modulo = Modulo::find($request['i_pk_id']);

		return $this->procesar($modulo, $request);
	}

	public function obtener($id, array $relaciones = [])
	{
        $modulo = Modulo::find($id);

		if (count($relaciones))
            $modulo->load($relaciones);

		return $modulo;
	}

	public function eliminar($id)
	{
        $modulo = Modulo::find($id);

		return $modulo->delete();
	}

	public function obtenerTodo(array $relaciones = [])
	{
        $modulo = Modulo::all();

		if (count($relaciones))
            $modulo->load($relaciones);

		return $modulo;
	}

	private function procesar($modulo, $request)
	{
        $modulo->vc_modulo = $request['vc_nombre'];
        $modulo->vc_redireccion = $request['vc_redireccion'];
        $modulo->vc_imagen = $request['vc_imagen'];
        $modulo->i_estado = $request['vc_estado'];
        $modulo->save();

        return $modulo;
	}
 
	public function obtenerNotasEstudiante($request){
		$usuario = Auth::id();
		return NotaPoli::where('i_fk_id_estudiante',$usuario)->get();
	} 

	public function obtenerNotasProfesor($request){
		return NotaPoli::get(); 
	}
 
	public function editarNota($request)
	{
		try{			
			$nota = NotaPoli::find($request->i_pk_id);
			$nota->d_nota_escenario_1 = $request->d_nota_escenario_1;
			$nota->d_nota_escenario_2 = $request->d_nota_escenario_2;
			$nota->d_nota_escenario_3 = $request->d_nota_escenario_3;
			return  $nota->save(); 
		}catch(Exception $e){
			return false;
		}
	}
	
	public function obtenerNotasEstudianteSeguroPorId($id){
		return NotaPoli::where('i_fk_id_estudiante',$id)->get();
	} 
	public function obtenerNotasEstudianteInseguroPorId($id){
		$resultSet = DB::connection('baseadmin')->select('SELECT * FROM tbl_notas_poli WHERE i_fk_id_estudiante = '.$id);
		return NotaPoli::hydrate($resultSet);
	} 		
}

