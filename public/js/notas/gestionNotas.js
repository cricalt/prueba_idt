var vURL = null;

jQuery(function(){
	vURL = $('#main').data('url'); 
	actualizarInputs();  

    $(document).on('keyup','.mayuscula',function(){ 
        $(this).val($(this).val().toUpperCase());
    });     

 
    $("#tablaNotas").on('click','.formulario-nota',function(e){   
        var nota= $(this).data('nota'); 
        $("#i_pk_id").val(nota.i_pk_id);
        $("#vc_estudiante").val(nota.estudiante.full_name);
        $("#vc_curso").val(nota.curso.vc_parametro_detalle);
        $("#d_nota_escenario_1").val(nota.d_nota_escenario_1);
        $("#d_nota_escenario_2").val(nota.d_nota_escenario_2);
        $("#d_nota_escenario_3").val(nota.d_nota_escenario_3);
        actualizarInputs();
        $("#modalActualizar").modal('show');
    });

    $(document).on('click','#guardar-nota',function(e){
        var datos = $(this).closest("form").serialize();        
        $.post(
            vURL+'/notas/editar-nota',
            datos, 
            function(data)
            {
                $("#modalActualizar").modal('hide');
                swal({
                    title: data.title,
                    html: data.message,
                    type: data.type,
                });                                   
            }
        );        
    });    

    function actualizarInputs(){
		$(".datepicker").datepicker({
			format: 'yyyy-mm-dd',
			weekStart: 1,
	    	language: 'es',
	    	autoclose: true,
	    });

	    $(".selectpicker").selectpicker('refresh');

		$(".btn-switch").bootstrapSwitch({
            onInit: function() {
                var valor = $(this).is(":checked") ? 1 : 0;
                $(this).val(valor);                
            },
            onSwitchChange: function(){
                var valor = $(this).is(":checked") ? 1 : 0;
                $(this).val(valor);
            }
        });   
    } 

    function validarForm(id){
        /* JQUERY VALIDATE*/

        // name validation
        var nombresregex = /^[a-zA-Z ]+$/;

        $.validator.addMethod("validname", function( value, element ) {
        return this.optional( element ) || nombresregex.test( value );
        }); 

        // valid email pattern
        var eregex = /^([a-zA-Z0-9_\\.\\-\\+])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/;
        
        $.validator.addMethod("validemail", function( value, element ) {
            return this.optional( element ) || eregex.test( value );
        });

        var numeroidentifiacionregex = /^(?=.*[0-9])([a-zA-Z0-9]+)$/;
        $.validator.addMethod("validIdentification", function( value, element ) {
            return this.optional( element ) || numeroidentifiacionregex.test( value );
        });

        var numeroregex = /^([0-9]+)$/;
        $.validator.addMethod("validNumber", function( value, element ) {
            return this.optional( element ) || numeroregex.test( value );
        });

        
        var requerido = {
            required: true
        } 
        var mrequerido = {
            required: 'Por favor diligencie este campo'
        }     
        var ruleSet1 = {
            required: true,
            validname: true,
            minlength: 3,
            maxlength: 50
        }

        var mruleSet1= {
            required: "Por favor diligencie este campo",
            validname: "El campo solo debe contener letras y/o espacios",
            minlength: "El campo es muy corto",
            maxlength: "Por favor no ingrese más de 50 caracteres"
        }   
        var ruleSet2 = {
            required: false,
            validname: true,
            minlength: 3,
            maxlength: 50
        };   

        var mruleSet2= {
            validname: "El campo solo debe contener letras y/o espacios",
            minlength: "El campo es muy corto",
            maxlength: "Por favor no ingrese más de 50 caracteres"
        }       

        $(id).validate({    
            rules:{
                i_cedula:{
                    required: true,
                    validIdentification: true,
                    maxlength: 20
                },
                i_fk_tipo_documento: requerido,
                name: ruleSet1,
                vc_segundo_nombre: ruleSet2,
                vc_primer_apellido: ruleSet1,
                vc_segundo_apellido: ruleSet2,
                email:{
                    validemail: true,
                    maxlength: 100
                },
                i_fk_ciudad: requerido,
                i_fk_genero: requerido,
                i_fk_eps: requerido,
                i_fk_area: requerido,
            },
            messages:{
                i_cedula: {
                    required: "Por favor ingrese un número de identificación",
                    validIdentification: "El número de identificación solo debe contener números y/o letras",
                    maxlength: "Por favor no ingrese más de 20 dígitos"
                },
                i_fk_tipo_documento: mrequerido,
                name: mruleSet1,
                vc_segundo_nombre: mruleSet2,
                vc_primer_apellido: mruleSet1,
                vc_segundo_apellido: mruleSet2,
                email: {
                    validemail: "Ingrese un Email válido",
                    maxlength: "Por favor no ingrese más de 100 caracteres" 
                },
                i_fk_ciudad: mrequerido,
                i_fk_genero: mrequerido, 
                i_fk_eps: mrequerido,  
                i_fk_area: mrequerido,      
            },
            errorPlacement : function(error, element) {
                $(element).closest('.form-group').find('.help-block').html(error.html());
            },
            highlight : function(element) {
                $(element).closest('.form-group').removeClass('has-info').addClass('has-error');
            }, 
            unhighlight: function(element, errorClass, validClass) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-info');
                $(element).closest('.form-group').find('.help-block').html('');
            }        
        });
        
    }      	
});
