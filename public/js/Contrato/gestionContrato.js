var vURL = null;
jQuery(function(){
	vURL = $('#main').data('url');


    $("#contenedorTabla").hide(); 
	$("#enlaceEdicionContrato").hide(); 
    


	$('[data-toggle="tooltip"]').tooltip();
    validarForm("#form-crear-contrato");
	actualizarInputs();  

    /*$("#contenedorCreacionContrato").on('change','.btn-switch',function(e){ 
        alert($(this).val());  
    });  */    

	
    $('#consultar-contratos-area').click(function (e) { 
    	var datos ={ 
    		'i_fk_id_area': $("#i_fk_id_area").val(),
    		'y_anio': $("#y_anio").val(),
            
    	}; 
        $.post(
            vURL+'/consultar-contratos-area',
            datos, 
            function(data)
            {
                $('#tablaContratosArea').html(data.html); 
                $("#contenedorTabla").show(); 
			    $("#table_contratos").DataTable({
			        responsive: true,
			        "pageLength": 10,
			        "language": {
			            "lengthMenu": "Ver _MENU_ registros por pagina",
			            "zeroRecords": "No hay información, lo sentimos.",
			            "info": "Mostrando pagina _PAGE_ de _PAGES_",
			            "infoEmpty": "No hay registros disponibles",
			            "infoFiltered": "(filtered from _MAX_ total records)",
			            "search": "Filtrar"
			        },
					dom: 'Bfrtip',
					buttons: [
					{
						extend: 'excel',

						title: 'Consulta de Contratos'
					}
					]		        
			    }).draw();                
            }
        );

    });  

    $("#tablaContratosArea").on('click',' .editar-contrato',function(e){   
    	var id= $(this).data('i_pk_id');
    	//console.log("hola :"+); 
        $.get(
            vURL+'/contratos/'+id+'/edit', 
            function(data)
            {
                $("#enlaceEdicionContrato").show(); 
                $("#enlaceEdicionContrato").trigger('click');	 
                $('#contenedorEdicionContrato').html(data.html); 
                //$("input[name='i_cedula']").trigger('blur');
                validarForm("#form-editar-contrato");                
                actualizarInputs();  

            }
        );    	
    });

    function actualizarInputs(){ 
        desHabilitarCamposCheckbox();
		$(".datepicker").datepicker({
			format: 'yyyy-mm-dd',
			weekStart: 1,
	    	language: 'es',
	    	autoclose: true,
	    });

	    $(".selectpicker").selectpicker('refresh');

		$(".btn-switch").bootstrapSwitch({
            onInit: function() {
                var valor = $(this).is(":checked") ? 1 : 0;
                $(this).val(valor);   
                habilitarCamposCheckbox(valor,$(this).attr('name'),$(this).closest('form').attr('id'));                             
            },
            onSwitchChange: function(){
                var valor = $(this).is(":checked") ? 1 : 0;
                $(this).val(valor);    
                habilitarCamposCheckbox(valor,$(this).attr('name'),$(this).closest('form').attr('id'));                
            }
        });

        // Asignación dinamica a inputs Hidden
        $("[id^='v_']").change(function(e){            
            var name=$(this).attr("id").replace("v_","");
            var idForm = $(this).closest('form').attr('id');
            $("#"+idForm+" input[name='"+name+"']").val($(this).val());
        });


    } 
     

    function validarForm(id){
        /* JQUERY VALIDATE*/

        // name validation
        var nombresregex = /^[a-zA-Z ]+$/;

        $.validator.addMethod("validname", function( value, element ) {
        return this.optional( element ) || nombresregex.test( value );
        }); 

        // valid email pattern
        var eregex = /^([a-zA-Z0-9_\\.\\-\\+])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/;
        
        $.validator.addMethod("validemail", function( value, element ) {
            return this.optional( element ) || eregex.test( value );
        });

        var numeroidentifiacionregex = /^(?=.*[0-9])([a-zA-Z0-9]+)$/;
        $.validator.addMethod("validIdentification", function( value, element ) {
            return this.optional( element ) || numeroidentifiacionregex.test( value );
        });

        var numeroregex = /^([0-9]+)$/;
        $.validator.addMethod("validNumber", function( value, element ) {
            return this.optional( element ) || numeroregex.test( value );
        });

        var requerido = {
            required: true
        } 
        var mrequerido = {
            required: 'Por favor diligencie este campo'
        }     
        var ruleSet1 = {
            required: true,
            validname: true,
            minlength: 3,
            maxlength: 50
        }

        var mruleSet1= {
            required: "Por favor diligencie este campo",
            validname: "El campo solo debe contener letras y/o espacios",
            minlength: "El campo es muy corto",
            maxlength: "Por favor no ingrese más de 50 caracteres"
        }   
        var ruleSet2 = {
            required: false,
            validname: true,
            minlength: 3,
            maxlength: 50
        };   

        var mruleSet2= {
            validname: "El campo solo debe contener letras y/o espacios",
            minlength: "El campo es muy corto",
            maxlength: "Por favor no ingrese más de 50 caracteres"
        }       

        $(id).validate({    
            rules:{
                i_fk_id_usuario: requerido,
                i_contrato: requerido,
                y_anio: requerido,
                d_fecha_inicial: requerido,
                d_fecha_final: requerido,
                i_fk_id_supervisor: requerido,
                i_fk_id_area: requerido,
            },
            messages:{
                i_fk_id_usuario: mrequerido,
                i_contrato: mrequerido,
                y_anio: mrequerido,
                d_fecha_inicial: mrequerido,
                d_fecha_final: mrequerido, 
                i_fk_id_supervisor: mrequerido,   
                i_fk_id_area: mrequerido,             
            },
            errorPlacement : function(error, element) {
                $(element).closest('.form-group').find('.help-block').html(error.html());
            },
            highlight : function(element) {
                $(element).closest('.form-group').removeClass('has-info').addClass('has-error');
            }, 
            unhighlight: function(element, errorClass, validClass) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-info');
                $(element).closest('.form-group').find('.help-block').html('');
            }        
        });
        
    }

    function desHabilitarCamposCheckbox(){
        $("select[name='i_fk_id_cedente']").closest('.form-group').hide();
        $("input[name='c_d_fecha_inicial']").closest('.form-group').hide();  
        $("input[name='d_fecha_final_prorroga']").closest('.form-group').hide();  
        $("input[name='d_fecha_terminacion']").closest('.form-group').hide();                                    
    }
    function habilitarCamposCheckbox(valor,elemento,form){
        var requerido = {
            required: true
        } 
        var mrequerido = {
            required: 'Por favor diligencie este campo'
        }   
                                     
        if(elemento=='i_cesion'){
            if(valor){   
 
                $("#"+form+" select[name='i_fk_id_cedente']").rules('add',requerido); 
                $("#"+form+" input[name='d_fecha_cesion']").rules('add',requerido); 
                $("#"+form+" select[name='i_fk_id_cedente']").closest('.form-group').show();
                $("#"+form+" input[name='d_fecha_cesion']").closest('.form-group').show(); 
            }
            else{
                $("#"+form+" select[name='i_fk_id_cedente']").rules('remove');   
                $("#"+form+" input[name='d_fecha_cesion']").rules('remove'); 
                $("#"+form+" select[name='i_fk_id_cedente']").val('').selectpicker('refresh');  
                $("#"+form+" input[name='d_fecha_cesion']").val('');
                $("#"+form+" select[name='i_fk_id_cedente']").closest('.form-group').hide();
                $("#"+form+" input[name='d_fecha_cesion']").closest('.form-group').hide();
            }
        }else if(elemento=='i_prorroga'){
            if(valor){  
                $("#"+form+" input[name='d_fecha_final_prorroga']").rules('add',requerido); 
                $("#"+form+" input[name='d_fecha_final_prorroga']").closest('.form-group').show();
            }
            else{   
                $("#"+form+" input[name='d_fecha_final_prorroga']").rules('remove');  
                $("#"+form+" input[name='d_fecha_final_prorroga']").val('');
                $("#"+form+" input[name='d_fecha_final_prorroga']").closest('.form-group').hide();
            }            
        }else if(elemento=='i_terminacion_anticipadas'){
            if(valor){  
                $("#"+form+" input[name='d_fecha_terminacion']").rules('add',requerido); 
                $("#"+form+" input[name='d_fecha_terminacion']").closest('.form-group').show();
            }
            else{   
                $("#"+form+" input[name='d_fecha_terminacion']").rules('remove');   
                $("#"+form+" input[name='d_fecha_terminacion']").val('');
                $("#"+form+" input[name='d_fecha_terminacion']").closest('.form-group').hide();
            }            
        }
    }

});