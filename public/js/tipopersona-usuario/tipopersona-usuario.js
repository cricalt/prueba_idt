 jQuery(document).ready(function () {
        var vURL = $('#main').data('url');

        var validateForm = $('#tipo_actividad');
        var validateRules = {
            'tipoPersona':    { required: true },
            'modulo':    { required: true },
        };

        var validateMessages = {};
        FormValidationMd.init(validateForm, validateRules, validateMessages, false);
     

        $('#TablaTipopersonaUsuario').delegate('input[data-target="#check"]','click',function (e) {  
                var id_tipo_persona = $(this).data('rel'); 
                var id_usuario_hiden = $('#id_usuario_hiden').val();
               
                if (this.checked == true){
                        $.get(
                            vURL+'/usuario-actividad/add/'+id_tipo_persona+'/'+id_usuario_hiden,
                            function(data)
                            {
                                $('#'+id_tipo_persona+'_id_label_ok').text(data);
                                setTimeout(function(){ 
                                    $('#'+id_tipo_persona+'_id_label_ok').text('');
                                }, 3000);
                            }
                        );
                  } else {
                        $.get(
                            vURL+'/usuario-actividad/remove/'+id_tipo_persona+'/'+id_usuario_hiden,
                            function(data)
                            {
                               $('#'+id_tipo_persona+'_id_label_dele').text(data);
                                setTimeout(function(){
                                    $('#'+id_tipo_persona+'_id_label_dele').text('');
                                }, 3000);
                            }
                        );
                  }

        });

        
    });