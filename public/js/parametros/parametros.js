var vURL = null;
jQuery(function($){
	vURL = $('#main').data('url');
	$("#contenedorTablaParametros").hide();   
	actualizarInputs();
	$("#obtener-parametros").click(function(){
		console.log($("#i_fk_id_parametro").val());
		$.get(
            vURL+'/detalle-parametro/'+$("#i_fk_id_parametro").val(),
            function(data)
            {
            	//console.log(data);
                $('#tablaParametros').html(data.html);                 
                $("#contenedorTablaParametros").show();                 
			    $("#table_parametros").DataTable({ 
			        responsive: true, 
			        "pageLength": 10,
			        "language": {
			            "lengthMenu": "Ver _MENU_ registros por pagina",
			            "zeroRecords": "No hay información, lo sentimos.",
			            "info": "Mostrando pagina _PAGE_ de _PAGES_",
			            "infoEmpty": "No hay registros disponibles",
			            "infoFiltered": "(filtered from _MAX_ total records)",
			            "search": "Filtrar"
			        },
					dom: 'Bfrtip',
					buttons: [
					{
						extend: 'excel',

						title: 'Consulta de Parametros'
					}
					]		        
			    }).draw();
            }
        );		
	});

	$("#modificar-parametro").click(function(){
		if($("#i_fk_id_parametro").val()!=""){

			$.get(
	            vURL+'/parametro/'+$("#i_fk_id_parametro").val()+'/edit',
	            function(data)
	            {
	            	console.log(data); 
	            	$('#contenedorFormEdit').html(data.html); 
	            	$("#modal-modificar-parametro").modal("show");
	            	actualizarInputs();
	            }
	        );
		}
			
	});

	$("#crear-parametro-detalle").click(function(){
			$("input[name='i_fk_id_parametro']").val($("#i_fk_id_parametro").val());
			$('.selectpicker').selectpicker('refresh');
	});


	/* Crear */
	$(".guardar-parametro-detalle").click(function(){
		var datos=$(this).closest("form").serialize();
		$.post(
            vURL+'/detalle-parametro',
            datos,
            function(data)
            { 
            	//console.log(data);
            	//alert(data.message);
                swal({
                    title: data.title,
                    html: data.message,
                    type: data.type,
                });
                $("#formulario-parametro-detalle").modal("hide");
	            $("#obtener-parametros").trigger("click");                                   
            }            
        );			

	});

	$("#contenedorFormEditDetalle").on('click',' .guardar-parametro-detalle',function(e){ 

		var datos=$(this).closest("form").serialize();
		var id= $(this).closest("form").find("input[name='i_pk_id']").val();
		$.post(
            vURL+'/detalle-parametro/'+id,
            datos,
            function(data)
            { 
            	//console.log(data);
            	//alert(data.message);
                swal({
                    title: data.title,
                    html: data.message,
                    type: data.type,
                });
                $("#modal-modificar-detalle").modal("hide");
	            $("#obtener-parametros").trigger("click");                                   
            }            
        );			

    });	

    $("#tablaParametros").on('click',' .modificar-parametro-detalle',function(e){   
    	//var id= $(this).data('id');
    	//console.log("hola :"+id); 
			$.get( 
	            vURL+'/detalle-parametro/'+$(this).data('id')+'/edit',
	            function(data)
	            {
	            	  
	            	$('#contenedorFormEditDetalle').html(data.html); 
	            	$("#modal-modificar-detalle").modal("show");
	            	$('.selectpicker').selectpicker('refresh');
	            	actualizarInputs();
	            }
	        );	
    });


	function actualizarInputs(){
		$(".btn-switch").bootstrapSwitch();	
		$('.selectpicker').selectpicker();	
	}

});
