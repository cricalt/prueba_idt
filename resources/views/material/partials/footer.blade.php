<div class="page-footer">
    <div class="page-footer-inner"> <script>document.write(new Date().getFullYear())</script> &copy; 
    	Hecho con <i class="fa fa-heart" aria-hidden="true" style="color: red;"></i> en
        <a target="_blank" href="javascript:;">{{ config('app.author') }}</a> - Oficina Asesora de Planeación y Tecnología &nbsp;|&nbsp;
        <a href="javascript:;" title="{{ config('app.author') }}" target="_blank">{{ config('app.name') }}</a>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>
</div>