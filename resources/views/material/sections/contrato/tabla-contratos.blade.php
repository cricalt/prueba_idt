<div class="card">
  
</div>
<table class='table table-hover' id='table_contratos' style='width: 100%'> 
  <thead>
    <tr>
      <th>Área</th>
      <th>Contrato - Año</th>
      <th>Contratista</th>
      <th>Cedula</th>
      <th>Fecha Inicial</th>
      <th>Fecha Fin</th>
      <th>Supervisor</th>  
      <th>Acciones</th>                        
      <th>Cesión</th>
      <th>Usuario<br> cedente</th>
      <th>Fecha cesión</th>
      <th>Prorroga</th>
      <th>Fecha Fin <br>Prorroga</th>
      <th>Terminación <br>Anticipada</th>
      <th>Fecha <br>Terminación</th>
    </tr>
  </thead>
  <tbody>
@foreach ($contratos as $contrato)  
  <tr>
    <td> {{ $contrato->area->vc_parametro_detalle }}   </td>
    <td> {{ $contrato->i_contrato }} - {{ $contrato->y_anio }} </td>
    <td> {{ $contrato->usuario->full_name }}</td>
    <td> {{ $contrato->usuario->i_cedula }}</td>
    <td> {{ $contrato->d_fecha_inicial }}</td>
    <td> {{ $contrato->d_fecha_final }} </td>
    <td> {{ $contrato->supervisor->full_name }}</td>
    <td> <button class="btn blue editar-contrato" data-i_pk_id="{{ $contrato->i_pk_id }}" data-toggle="tooltip" title="Editar Contrato"><i class="icon-pencil"></i></button></td> 
    <td> @if($contrato->i_cesion) SI @else NO  @endif</td>
    <td> @if(!is_null($contrato->cedente)) {{ $contrato->cedente->full_name }} @endif</td>
    <td> {{ $contrato->d_fecha_cesion }}</td>
    <td> @if($contrato->i_prorroga) sI @else NO  @endif</td>
    <td> {{ $contrato->d_fecha_final_prorroga }}</td>
    <td> @if($contrato->i_terminacion_anticipadas) sI @else NO  @endif</td>
    <td> {{ $contrato->d_fecha_terminacion }}</td>
  </tr>
@endforeach 
</tbody>
</table>