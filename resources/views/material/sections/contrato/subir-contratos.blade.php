@extends('material.layouts.public')

 
@push('styles')

    <!-- Select picker -->
    <link href="{{ asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
    <div class="col-md-12" id="main" data-url="{{URL::to('/')}}">
        <div class="col-md-12 ">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet light">
                <div class="portlet-title tabbable-line">
                    <div class="caption">
                        <i class="icon-pin font-blue-madison"></i>
                        <span class="caption-subject bold font-blue-madison uppercase"> Carga masiva de Contratos </span>                       
                    </div> 
                    <div class="actions">           
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""></a>
                    </div>

                </div>
                <div class="portlet-body">
                    {{ Form::open(['role' => 'form','id' => 'form-subir-contrato', 'class'=> 'm-portlet', 'method' => 'POST' , 'url' => route('contratos.store'), 'enctype' => 'multipart/form-data' ]) }}

                        {!!Form::file('file',null,['class'=>'form-control'])!!} 

                        <div class="form-actions noborder">

                        {{ Form::submit('Guardar', ['class' => 'form-control btn green']) }}

                        </div>

                    {{ Form::close() }} 
                    {{-- aqui va el formulario --}}
                </div>
            </div>
        </div>            
    </div>
@endsection

@push('plugins')
    <!-- Validation Scripts -->
    <script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-validation/js/localization/messages_es.js') }}" type="text/javascript"></script>
   
    <script src="{{ asset('assets/pages/scripts/form-validation-md.js') }}" type="text/javascript"></script>

    <!-- Select picker -->
    <script src="{{ asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
@endpush


@push('functions')
<script>
    jQuery(document).ready(function () {
        @if (session('message'))
            swal({
                title: "{{ session('title') }}",
                text: "{{ session('message') }}",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-success",
                type: "{{ session('type') }}",
            });
        @endif
        
    });
</script>


@endpush