
<h6 class="caption-subject bold uppercase font-green-haze"><i class="fa fa-user-circle font-green-haze"></i> Datos del Contratista</h6>
<div class="form-body">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group form-md-line-input has-info">
                {!!Form::select('v_i_fk_id_usuario',$usuarios,isset($contrato) ? $contrato->usuario->id : null,['class'=>'form-control selectpicker','id'=>'v_i_fk_id_usuario','data-live-search'=>'true','title'=>'Seleccione el Contratista','disabled' => isset($contrato),'data-size'=>'8'])!!} 
                <label for="v_i_fk_id_usuario">Contratista                    
                </label>
                <span class="help-block">                   
                </span>
                {!! Form::hidden('i_fk_id_usuario', isset($contrato) ? $contrato->usuario->id : null) !!}
            </div>                           
        </div>   
        @if(!isset($contrato))  
        <div class="col-md-6">
            <div class="form-group form-md-line-input has-info">
                <div class="col-xs-6">
                    <small>¿No encuentra el Contratista? use la opción de:</small> 
                </div>
                <div class="col-xs-6">
                    <a href="{{ URL::route('gestionar-usuarios') }}" class="btn green">
                        <span class="menu-text">Crear Contratista</span>
                    </a>
                </div>
            </div>
        </div>       
        @endif
    </div>      
</div>
<h6 class="caption-subject bold uppercase font-green-haze"><i class="fa fa-wrench font-green-haze"></i> Datos del Contrato</h6>
<div class="form-body">
    <div class="row">
        <div class="col-md-3">
            <div class="form-group form-md-line-input has-info">
                <div class="input-icon">
                    {!!Form::number('v_i_contrato', isset($contrato) ? $contrato->i_contrato : null,['class'=>'form-control','id'=>'v_i_contrato','placeholder'=>'# de contrato','disabled' => isset($contrato)])!!} 
                    <label for="v_i_contrato">Número de Contrato (*)</label>
                    <span class="help-block"></span>
                    <i class="fa fa-bell-o"></i>
                    {!! Form::hidden('i_contrato', isset($contrato) ? $contrato->i_contrato : null) !!}
                </div>
            </div>
        </div>
        <div class="col-md-3"> 
            <div class="form-group form-md-line-input has-info">
                {!!Form::select('v_y_anio',$anios,isset($contrato) ? $contrato->y_anio : null,['class'=>'form-control selectpicker','id'=>'v_y_anio','data-live-search'=>'true','title'=>'Indique el año','disabled' => isset($contrato),'data-size'=>'8'])!!}                                
                <label for="v_y_anio">Año de contrato (*)</label>
                <span class="help-block"></span>
                {!! Form::hidden('y_anio', isset($contrato) ? $contrato->y_anio : null) !!}
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group form-md-line-input has-info">
                <div class="input-icon">    
                    {!!Form::text('d_fecha_inicial',null,['class'=>'form-control datepicker', 'readonly'=>'readonly'])!!}                                                             
                    <label for="d_fecha_inicial">Fecha de Inicio (*)</label>
                    <i class="fa fa-calendar"></i>
                    <span class="help-block"></span>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group form-md-line-input has-info">
                <div class="input-icon">    
                    {!!Form::text('d_fecha_final',null,['class'=>'form-control datepicker','readonly'=>'readonly'])!!}                                                             
                    <label for="d_fecha_final">Fecha de Fin (*)</label>
                    <i class="fa fa-calendar"></i>
                    <span class="help-block"></span>
                </div>
            </div>
        </div>                                                    
          
    </div> 
    <div class="row">               
        <div class="col-md-6">
            <div class="form-group form-md-line-input has-info">
                {!!Form::select('i_fk_id_supervisor',$usuarios,null,['class'=>'form-control selectpicker','data-live-search'=>'true','title'=>'Indique el Supervisor','data-size'=>'8','data-live-search'=>'true'])!!} 
                <label for="i_fk_id_supervisor">Supervisor (*)</label>
                <span class="help-block"></span>
            </div>
        </div>      
        <div class="col-md-3">
            <div class="form-group form-md-line-input has-info">
                {!!Form::select('i_fk_id_area',$areas,null,['class'=>'form-control selectpicker','data-live-search'=>'true','title'=>'Indique el área','data-size'=>'8'])!!}  
                <label for="i_fk_id_area">Área (*)</label>
                <span class="help-block"></span>
            </div>
        </div>      
        <div class="col-md-3">
            <div class="form-group form-md-line-input has-info">
                {!!Form::select('i_fk_id_sub_area',$sub_areas,null,[
                    'class'=>'form-control selectpicker',
                    'data-live-search'=>'true',
                    'title'=>'Indique el sub área',
                    'data-size'=>'8'
                ])!!}  
                <label for="i_fk_id_sub_area">Sub Área</label>
                <span class="help-block"></span>
            </div>
        </div>               
    </div>       
    <div class="row">
        <div class="col-md-3 form-group form-md-line-input has-info input-check">
            <div class="input-icon">
                <label for="i_prorroga">¿Tiene prorroga? (*)</label>   
                {!!Form::checkbox('i_prorroga',null, isset($contrato) ? $contrato->i_prorroga : 0, ['class'=>'btn-switch','data-on-color'=>'success','data-on-text'=>'SI','data-off-color'=>'danger','data-off-text'=>'NO'])!!} 
                <span class="help-block"></span>                               
            </div>   
        </div>
        <div class="col-md-3">
            <div class="form-group form-md-line-input has-info">
                <div class="input-icon">    
                    {!!Form::text('d_fecha_final_prorroga',null,['class'=>'form-control datepicker'])!!}                                                             
                    <label for="d_fecha_final_prorroga">Fecha de Fin Prorroga (*)</label>
                    <i class="fa fa-calendar"></i>
                    <span class="help-block"></span>
                </div>
            </div>
        </div>                                                              
        <div class="col-md-3 form-group form-md-line-input has-info input-check">
            <div class="input-icon"> 
                <label for="i_terminacion_anticipadas">¿Tiene terminación anticipada? (*)</label>
                {!!Form::checkbox('i_terminacion_anticipadas',null,isset($contrato) ? $contrato->i_terminacion_anticipadas : 0, ['class'=>'btn-switch','data-on-color'=>'success','data-on-text'=>'SI','data-off-color'=>'danger','data-off-text'=>'NO'])!!}     
                <span class="help-block"></span>                           
            </div>                                                            
        </div> 
        <div class="col-md-3">
            <div class="form-group form-md-line-input has-info">
                <div class="input-icon">    
                    {!!Form::text('d_fecha_terminacion',null,['class'=>'form-control datepicker', 'readonly'=>'readonly'])!!}                                                             
                    <label for="d_fecha_terminacion">Fecha Terminación Ant (*)</label>
                    <i class="fa fa-calendar"></i>
                    <span class="help-block"></span>
                </div>
            </div>
        </div> 
    </div>                                                
    <div class="row">
        <div class="col-md-2 form-group form-md-line-input has-info input-check">
            <div class="input-icon"> 
                <label for="i_cesion">¿Tiene cesión? (*)</label>
                {!!Form::checkbox('i_cesion',null,isset($contrato) ? $contrato->i_cesion : 0,['class'=>'form-control btn-switch','data-on-color'=>'success','data-on-text'=>'SI','data-off-color'=>'danger','data-off-text'=>'NO','id'=>'i_cesion','disabled' => ((isset($contrato) && $contrato->i_cesion) || !isset($contrato) )])!!}     
                <span class="help-block"></span>                            
            </div>                                                            
        </div>
        <div class="col-md-7"> 
            <div class="form-group form-md-line-input has-info">
                {!!Form::select('i_fk_id_cedente',$usuarios,null,['class'=>'form-control selectpicker','data-live-search'=>'true','title'=>'Indique el Cedente','disabled' => (isset($contrato) && $contrato->i_cesion)])!!} 
                <label for="i_fk_id_cedente">Cedente (*)</label>
                <span class="help-block"></span>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group form-md-line-input has-info">
                <div class="input-icon">    
                    {!!Form::text('d_fecha_cesion',null,['class'=>'form-control datepicker', 'readonly'=>'readonly','disabled' => (isset($contrato) && $contrato->i_cesion)])!!}                                                             
                    <label for="d_fecha_cesion">Fecha de Inicio Cesión (*)</label>
                    <i class="fa fa-calendar"></i>
                    <span class="help-block"></span>
                </div>
            </div>
        </div>    
    </div>    
</div>
<div class="form-actions noborder">
     {{ Form::submit('Guardar', ['class' => 'form-control btn green']) }}
</div>

 