@extends('material.layouts.'.$theme)
 
 
@push('styles')

<!-- Modal Styles -->
    <link href="{{ asset('assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('assets/pages/css/blog.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Select picker -->
    <link href="{{ asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- Radio Styae -->
    <link href="{{ asset('assets/global/plugins/icheck/skins/all.css') }}" rel="stylesheet" type="text/css" />

    <!-- Datatables Styles -->
    <link href="{{ asset('bower_components/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('bower_components/datatables.net-buttons-dt/css/buttons.dataTables.min.css') }}" rel="stylesheet" type="text/css"> 

    <!-- DatePicker -->
    <link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet"> 

@endpush

@section('content')
    <div class="col-md-12" id="main" data-url="{{URL::to('/')}}">
        <div class="col-md-12 ">
            <!-- BEGIN Portlet PORTLET-->
            <div class="portlet light">
                <div class="portlet-title tabbable-line">
                    <div class="caption">
                        <i class="icon-pin font-blue-madison"></i>
                        <span class="caption-subject bold font-blue-madison uppercase"> Gestión de Notas </span>                       
                    </div> 
                    <div class="actions">           
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="portlet_tab1">                         
                            <div class="portlet light bordered" id="contenedorTabla">
                                <div id="tablaNotas">
                                    <div class="card">  
                                        <table class='table table-hover' id='table_notas' style='width: 100%'> 
                                          <thead>
                                            <tr>
                                              <th>Usuario</th>
                                              <th>Curso</th> 
                                              <th>Nota Escenario 1</th>
                                              <th>Nota Escenario 2</th>
                                              <th>Nota Escenario 3</th>
                                              <th>Promedio</th>
                                              <th>Acciones</th>                        
                                            </tr>
                                          </thead>
                                          <tbody>
                                          @foreach ($notas as $nota)  
                                            <tr> 
                                              <td> {{ $nota->estudiante->full_name }}</td>
                                              <td> {{ $nota->curso->vc_parametro_detalle }}</td>
                                              <td> {{ $nota->d_nota_escenario_1 }}</td>
                                              <td> {{ $nota->d_nota_escenario_2 }}</td>
                                              <td> {{ $nota->d_nota_escenario_3 }}</td>
                                              <td> {{ $nota->promedio }}</td>
                                              <td>
                                                  @if ($editor)  
                                                    {{--  <pre>{{ print_r(json_encode($nota->toArray()),true) }}</pre>--}}
                                                    <button class="btn green formulario-nota fa fa-pencil" data-nota="{{ json_encode($nota) }}" data-id="{{ $nota->i_pk_id }}" data-toggle="tooltip" title="Editar"></button> 
                                                  @endif 
                                              </td> 
                                            </tr>
                                          @endforeach 
                                        </tbody>
                                        </table>
                                      </div>                                    
                                </div>  
                            </div>                             
                        </div>
                    </div>
                </div>
            </div>
        </div>            
    </div>

    <div id="modalActualizar" class="modal container fade" tabindex="-1" aria-hidden="false" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">
                <i class="icon-settings font-green-haze"></i>
                <span class="caption-subject bold uppercase">Actualizar Nota</span>
            </h4>
        </div> 
        <div class="modal-body">
        {!!Form::open(['route'=>['editar-nota'],'method'=>'POST','role'=>'form','id'=>'form-editar-nota','enctype'=>'multipart/form-data'])!!} 
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-md-6">
                        {!!Form::hidden('i_pk_id', null,['id'=>'i_pk_id'])!!} 
                        <div class="form-group form-md-line-input has-info">
                            <div class="input-icon">
                                {!!Form::text('vc_estudiante', null,['class'=>'form-control','id'=>'vc_estudiante','disabled'=>'disabled'])!!} 
                                <label for="vc_estudiante">Estudiante</label>
                                <span class="help-block"></span>
                                <i class="fa fa-sort-numeric-asc"></i>
                            </div> 
                        </div>                                   
                    </div>
                    <div class="col-md-6">
                        <div class="form-group form-md-line-input has-info">
                            <div class="input-icon">
                                {!!Form::text('vc_curso', null,['class'=>'form-control','id'=>'vc_curso','disabled'=>'disabled'])!!} 
                                <label for="vc_curso">Curso</label>
                                <span class="help-block"></span> 
                                <i class="fa fa-sort-numeric-asc"></i>
                            </div> 
                        </div>                                   
                    </div>                      
                </div>
                <div class="row mt-2">
                    <div class="col-md-4">
                        <div class="form-group form-md-line-input has-info">
                            <div class="input-icon">
                                {!!Form::text('d_nota_escenario_1', null,['class'=>'form-control','placeholder'=>'Nota Escenario 1 (*)','id'=>'d_nota_escenario_1'])!!} 
                                <label for="d_nota_escenario_1">Nota Escenario 1 (*)</label>
                                <span class="help-block"></span>
                                <i class="fa fa-sort-numeric-asc"></i>
                            </div> 
                        </div>                                   
                    </div> 
                    <div class="col-md-4">
                        <div class="form-group form-md-line-input has-info">
                            <div class="input-icon">
                                {!!Form::text('d_nota_escenario_2', null,['class'=>'form-control','placeholder'=>'Nota Escenario 2 (*)','id'=>'d_nota_escenario_2'])!!} 
                                <label for="d_nota_escenario_2">Nota Escenario 2 (*)</label>
                                <span class="help-block"></span>
                                <i class="fa fa-sort-numeric-asc"></i>
                            </div> 
                        </div>                                   
                    </div>
                    <div class="col-md-4">
                        <div class="form-group form-md-line-input has-info">
                            <div class="input-icon">
                                {!!Form::text('d_nota_escenario_3', null,['class'=>'form-control','placeholder'=>'Nota Escenario 3 (*)','id'=>'d_nota_escenario_3'])!!} 
                                <label for="d_nota_escenario_3">Nota Escenario 3 (*)</label>
                                <span class="help-block"></span>
                                <i class="fa fa-sort-numeric-asc"></i>
                            </div> 
                        </div>                                   
                    </div>                                                                                                     
                </div>
                <div class="row mt-2">
                    <div class="col-md-12">                
                        <button type="button" class="btn blue btn-block" id="guardar-nota">Guardar</button>
                    </div>
                </div>
            </div>
        {!!Form::close()!!}     
        </div>
        <div class="modal-footer">

        </div>
    </div>    
@endsection

@push('plugins')
    <!-- Validation Scripts -->
    <script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/jquery-validation/js/localization/messages_es.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/form-validation-md.js') }}" type="text/javascript"></script>

    <script src="{{ asset('assets/pages/scripts/general-request.js') }}" type="text/javascript"></script>
<!-- Modal Scripts -->
    <script src="{{ asset('assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js') }}" type="text/javascript"></script>
<!-- Select picker -->
    <script src="{{ asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/i18n/es.js"></script> --}}
<!-- Radio Script -->
    <script src="{{ asset('assets/global/plugins/icheck/icheck.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/pages/scripts/form-icheck.min.js') }}" type="text/javascript"></script>
<!-- Datatables Scripts -->   
    <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('bower_components/datatables.net-responsive-bs/js/responsive.bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/dataTables.buttons.min.js') }}"  type="text/javascript" ></script>
    <script src="{{ asset('bower_components/jszip/dist/jszip.min.js') }}"  type="text/javascript" ></script>
    <script src="{{ asset('bower_components/datatables.net-buttons/js/buttons.html5.min.js') }}" type="text/javascript" ></script>  

    <!-- DatePicker -->
    <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script> 

    <script src="{{ asset('js/notas/gestionNotas.js?v=2020-07-15') }}" type="text/javascript"></script>
@endpush


@push('functions')
<script>
    jQuery(document).ready(function () {
        @if (session('message'))
            swal({
                title: "{{ session('title') }}",
                text: "{{ session('message') }}",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-success",
                type: "{{ session('type') }}",
            });
        @endif
        {{-- cuando no se envía por redirect --}}
        @if (isset($message))
            swal({
                title: "{{ $title }}",
                text: "{{ $message }}",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-success",
                type: "{{ $type }}",
            });
        @endif        
        @if(isset($usuario))   
            $('#modalActualizar').modal('show');
        @elseif($errors->any())
            $('#modalCrear').modal('show');
        @endif       
    });
</script>


@endpush