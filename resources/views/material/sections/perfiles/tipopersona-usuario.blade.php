@extends('material.layouts.dashboard')


@push('styles')
<!-- Toaster Styles -->
<link href="{{ asset('assets/global/plugins/bootstrap-toastr/toastr.min.css') }}" rel="stylesheet" type="text/css" />
<!-- Datatables Styles -->
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
<!-- Tree View -->
<link href="{{ asset('assets/global/plugins/jstree/dist/themes/default/style.min.css') }}" rel="stylesheet" type="text/css" />
<!-- Modal Styles -->
<link href="{{ asset('assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css') }}" rel="stylesheet" type="text/css" />
<!-- SweetAlert Styles -->
<link href="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />
<!-- Toaster Styles -->
<link href="{{ asset('assets/global/plugins/bootstrap-toastr/toastr.min.css') }}" rel="stylesheet" type="text/css" />
<!-- Select picker -->
<link href="{{ asset('assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('page-title', '')


@section('page-description', '')

@section('content')
    <div class="col-md-12">
      @php   
        if($clase==268)  {
          $titulo = 'Asignar Tipo Persona (Rol) a Usuario';
          $tituloTabla = 'Tipo Persona (Rol)';
        }else{
          $titulo = 'Asignar Cargo a Usuario';
          $tituloTabla = 'Cargo';
        }
      @endphp

        @component('themes.bootstrap.components.portlet', ['icon' => 'fa fa-building-o', 'title' => $titulo])

            <div class="row">
                <div class="content" id="main" class="row" data-url="{{ url('/') }}" ></div>
                <div class="col-md-6 col-md-offset-3">
              
                        {{ Form::open(['class' => 'horizontal-form', 'novalidate' => 'novalidate', 'id' => 'tipo_actividad', 'method' => 'POST' , 'url' => route('tipopersona.usuario.show') ]) }}

                            <div class="form-body">
                                {{ Form::hidden('i_fk_id_clase',$clase,['id'=>'i_fk_id_clase']) }}
                                {{--                                 
                                <div class="row">
                                    <div class="col-md-12 form-group form-md-line-input has-success">
                                            {!! Field::text('i_cedula', ['id' => 'create_nombre', 'required', 'label' => 'Cédula'], ['help' => 'Digita el numero de cédula del usuario.', 'icon' => 'icon-tag']) !!} 
                                    </div>  
                                </div>  
                                --}} 
                              <div class="row">
                                <div class="col-md-12">
                                    <div class="input-group form-md-line-input has-info">
                                        {!!Form::select('user_id',$usuarios, is_object($user) ? $user->id : null ,[
                                            'class'=>'form-control selectpicker ',
                                            'data-live-search'=>'true',
                                            'title'=>'Seleccione el usuario',
                                            'data-size'=>'8',
                                            'required'=>'required',
                                            'id'=>'user_id',
                                            'data-live-search'=>'true'
                                            ])!!} 
                                        <span class="help-block"></span>
                                        <div class="input-group-btn">
                                            <button type="submit" class="btn green" id="btn_edit"> 
                                                <i class="fa fa-user-circle" aria-hidden="true"></i>
                                                Ver perfiles
                                            </button>
                                        </div>                    
                                    </div>                                   
                                </div> 
                              </div>                                                               
                            </div>                                

                            {{--                             
                            <div class="form-actions text-right">
                                {{ Form::submit('Generar búsqueda', ['class' => 'btn green', 'id' => 'btn_edit']) }}
                            </div> 
                            --}}

                        {{ Form::close() }}
                    </form>
                </div>
            </div>
           
        @endcomponent


        @if($user!=null)
            @component('themes.bootstrap.components.portlet', ['icon' => 'fa fa-forumbee', 'title' => 'Resultados búsqueda'])
               <div class="row">
                   
                   <div class="col-xs-12 col-md-12s text-center">
                        <h2><small> <span class="glyphicon glyphicon-user"></span> <b> USUARIO:</b>  <br>{{  $user['full_name'] }}</small></h2>
                   </div>
                   
                   <div class="col-xs-12 col-md-12">
                       <br><br><br>
                   </div>

                   <div class="col-xs-12 col-md-12">
                    <input type="hidden" name="id_usuario_hiden"  id="id_usuario_hiden" value="{{$user->id}}">
                    
                    
                   </div>
                   <div class="col-xs-12 col-md-12">
                       <table class="display responsive no-wrap table table-min table-striped " cellspacing="0" id='TablaTipopersonaUsuario'>
                          <thead>
                            <tr>
                              <th>ID</th>
                              <th>{{ $tituloTabla }}</th>
                              <th>Módulo</th>
                              <th>Estado</th>
                            </tr>
                          </thead>
                          <tfoot>
                            <tr class="estado" >
                              <th>ID</th>
                              <th>Tipo Persona</th>
                              <th>Módulo</th>
                              <th>Estado</th>
                            </tr>
                          </tfoot>
                          <tbody>
                            @foreach($tipoPersonas as $tipoPersona)
                                {{-- <pre>{{ print_r($tipoPersona,true) }}</pre> --}}
                                
                                <?php $estado=""; ?>
                                @foreach($user->tiposPersona as $tiposPersona)
                                    @if($tipoPersona['i_pk_id'] == $tiposPersona['i_pk_id'])
                                        <?php $estado="checked"; ?>
                                    @endif
                                @endforeach

                                <tr data-row="{{ $tipoPersona['i_pk_id'] }}">
                               
                                    <td>
                                        {{ $tipoPersona['i_pk_id'] }}                                  
                                    </td>
                                    <td>
                                        {{ $tipoPersona['vc_tipo'] }}                                  
                                    </td>

                                    <td>
                                        {{ is_object($tipoPersona->modulos) ? $tipoPersona->modulos->vc_modulo : ''   }}                           
                                    </td>

                                    <td>
                                        <div class="mt-checkbox-list">
                                            <label class="mt-checkbox"> Activo
                                                
                                                <input type="checkbox" 
                                                value="{{ $tipoPersona['i_pk_id'] }}" 
                                                name="test"  
                                                {{ $estado }} 
                                                data-target="#check" 
                                                data-rel="{{$tipoPersona['i_pk_id']}}">
                                                
                                                <span></span>
                                            </label>    
                                            <span class="label label-success" id="{{$tipoPersona['i_pk_id']}}_id_label_ok"></span>
                                            <span class="label label-danger" id="{{$tipoPersona['i_pk_id']}}_id_label_dele"></span>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                          </tbody>
                        </table>
                   </div>
               </div> 
            @endcomponent
        @endif
    </div>




@endsection


@push('functions')
<!-- Toastr Scripts -->
<script src="{{ asset('assets/global/plugins/bootstrap-toastr/toastr.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/ui-toastr.js') }}" type="text/javascript"></script>
<!-- Datatables Scripts -->
<script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/table-datatable.js') }}" type="text/javascript"></script>
<!-- Modal Scripts -->
<script src="{{ asset('assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js') }}" type="text/javascript"></script>
<!-- SweetAlert Scripts -->
<script src="{{ asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
<!-- Validation Scripts -->
<script src="{{ asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/jquery-validation/js/localization/messages_es.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/form-validation-md.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/general-request.js') }}" type="text/javascript"></script>
<!-- Select picker -->
    <script src="{{ asset('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
<!-- My Script -->
<script src="{{ asset('js/tipopersona-usuario/tipopersona-usuario.js?v=0.0') }}" type="text/javascript"></script>

@endpush


@push('functions')
<script>
    jQuery(document).ready(function () {
        @if (session('message'))
            swal({
                title: "{{ session('title') }}",
                text: "{{ session('message') }}",
                buttonsStyling: false,
                confirmButtonClass: "btn btn-success",
                type: "{{ session('type') }}",
            });
        @endif
    });
</script>
@endpush