<div class="card">  
  <table class='table table-hover' id='table_usuarios' style='width: 100%'> 
    <thead>
      <tr>
        <th>Tipo de Documento</th>
        <th>Área</th> 
        <th>Documento</th>
        <th>Nombre</th>
        <th>Fecha Nacimiento</th>
        <th>Email</th>
        <th>Acciones</th>                        
        <th>Teléfono</th>  
      </tr>
    </thead>
    <tbody>
    @foreach ($usuarios as $usuario)  
      <tr>
        <td> {{ $usuario->i_fk_tipo_documento }}</td>
        <td> {{ $usuario->area->vc_parametro_detalle }}</td>
        <td> {{ $usuario->i_cedula }}</td>
        <td> {{ $usuario->full_name }}</td>
        <td> {{ $usuario->dt_fecha_nacimiento }}</td>
        <td> {{ $usuario->email }} </td>
        <td> 
            <a class="btn blue fa fa-pencil" href="{{ url('/usuarios/'.$usuario->id.'/edit') }}"></a>
            <button class="btn green reset-password fa fa-key" data-id="{{ $usuario->id }}" data-toggle="tooltip" title="Resetear Contraseña"></button> 
        </td> 
        <td> {{ $usuario->vc_telefono }}</td>
      </tr>
    @endforeach 
  </tbody>
  </table>
</div>