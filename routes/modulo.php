<?php

Route::group(['prefix' => 'modulo', 'middleware' => 'auth'], function()
{
   
    Route::get('/', [
        'uses' => '\App\Modulos\Modulo\Controllers\ModuloController@inicio',
        'as' => 'costos_municipios'
    ]);

    Route::post('/create', [
        'uses' => '\App\Modulos\Modulo\Controllers\ModuloController@crear',
        'as' => 'create_modulo'
    ]);

    Route::get('/data', [
        'uses' => '\App\Modulos\Modulo\Controllers\ModuloController@data',
        'as' => 'table_school'
    ]);

    Route::put('/save', [
        'uses' => '\App\Modulos\Modulo\Controllers\ModuloController@actualizar',
        'as' => 'save_modulo'
    ]);

});


Route::group(['prefix' => 'notas', 'middleware' => 'auth'], function()
{
    Route::get('/estudiante', [
        'uses' => '\App\Modulos\Modulo\Controllers\ModuloController@notasEstudiante',
        'as' => 'notas-estudiante'
    ]);

    Route::get('/profesor', [
        'uses' => '\App\Modulos\Modulo\Controllers\ModuloController@notasProfesor',
        'as' => 'notas-estudiante'
    ]); 
    
    Route::post('/editar-nota', [
        'uses' => '\App\Modulos\Modulo\Controllers\ModuloController@editarNota',
        'as' => 'editar-nota'
    ]);      
});    

Route::get('/notas-publico-seguro/{id}', [
    'uses' => '\App\Modulos\Modulo\Controllers\ModuloController@notasPublicoSeguro',
    'as' => 'notas-publico-seguro'
]);

Route::get('/notas-publico-inseguro/{id}', [
    'uses' => '\App\Modulos\Modulo\Controllers\ModuloController@notasPublicoInseguro',
    'as' => 'notas-publico-inseguro'
]);

