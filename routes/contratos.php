<?php

    Route::group(['middleware' => 'auth'], function(){
    	$controller = "\\App\\Modulos\\Contrato\\Controllers\\";


        Route::resource('contratos', $controller .'ContratosController'); 


		/*Route::get('/gestionar-contratos', [  
            'uses' => $controller .'ContratosController@index' ,
            'as' => 'gestionar-contratos'
        ]);*/	    

        Route::post('/consultar-contratos-area', [  
            'uses' => $controller .'ContratosController@consultarContratosArea' ,
            'as' => 'consultar-contratos-area' 
        ]);	       

        Route::get('/subir-contratos',function(){
            return view('material.sections.contrato.subir-contratos');
        });          

        Route::get('/gestionar-contratos',[
            'uses' => $controller .'ContratosController@inicio',
             'as' =>  'gestionar-contratos',
        ]);

        Route::get('/gestion-obligaciones',function(){
            return '<h1>Empiece a programar!</h1>';
        }); 

        Route::get('/asociar-obligaciones',function(){
            return '<h1>Siga programando!</h1>';
        });         

        

    });